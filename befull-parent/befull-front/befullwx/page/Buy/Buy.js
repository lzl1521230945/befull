// 引入coolsite360交互配置设定
require('coolsite.config.js');
// 获取全局应用程序实例对象
var app = getApp();
// 创建页面实例对象
Page({
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    circular: true,
    isShow: false,
    warning: false,
    warnDes: "",
    number: 1,
    postData: {},
  },
  postData: {},

  onLoad: function(option) {
    console.log(option);
    wx.request({
      url: "http://befull.mcaa.win/foodSelectById.action?foodId="+option.id,
      dataType: "json",
      method: "post",
      header: {
        'content-type': "application/x-www-form-urlencoded" // 默认值
      },
      success: res => {
        console.log(res.data.data);
        this.setData({
          foodsAll: res.data.data,
        });
      }
    })
  },
  onShareAppMessage: function () {
    var id = wx.getStorageSync('current_id');
    var path = '/pages/start/start?id=' + id + '';
    return {
      title: "饱了吗点餐小程序",
      path: path
    }
  },
  // 触发coolsite360交互事件
  onShow() {
    app.coolsite360.onShow(this);
  },
  tap_3013baf2: function (e) {
    app.coolsite360.fireEvent(e, this);
  },
  tap_d5808da3: function (e) {
    app.coolsite360.fireEvent(e, this);
  },


  // 显示隐藏购物车
  showCartDetail: function () {
    wx.switchTab({
      url: '../foods-cart/foods-cart'
    })
   /* this.setData({
      showCartDetail: !this.data.showCartDetail
    });
  },
  hideCartDetail: function () {
    this.setData({
      showCartDetail: false
    });*/
  },

  // 抽屉显示和隐藏
  setModalStatus: function (e) {
    console.log("设置显示状态，1显示0不显示", e.currentTarget.dataset.status);
    var animation = wx.createAnimation({
      duration: 388,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export()
    })
    if (e.currentTarget.dataset.status == 1) {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
    /* 抽屉弹出动画 */
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation
      })
      if (e.currentTarget.dataset.status == 0) {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)
  },

  // 抽屉弹出信息的属性值
  getChecked: function (e) {
   
      var that = this,
        haveCheckedProp = " " + e.currentTarget.dataset.value;
    this.setData({
      haveCheckedProp: haveCheckedProp
    })
  },
  getCheckeds: function (e) {

    var that = this,
      haveCheckedProps = " " + e.currentTarget.dataset.value;
    this.setData({
      haveCheckedProps: haveCheckedProps
    })
  },

  goToCounter: function () {

    var that = this;

      // length = that.data.item.property.length,   //属性num
      // objLength = common.objLength(that.data.postData);   //已选择属性num
    
   
    //组建购物车
    var shopCarInfo = this.bulidShopCarInfo();
    this.setData({
      shopCarInfo: shopCarInfo,
    });

    // 写入本地存储
    wx.setStorage({
      key: "shopCarInfo",
      data: shopCarInfo
    })
      
    wx.navigateTo({
      url: "/page/foods-cart/foods-cart",
      success: function (res) {
      }
    })
      
    wx.showToast({
      title: '加入购物车成功',
      icon: 'success',
      duration: 2000
    })
    
  },

  addNum: function () {
    var that = this,
      num = that.data.number;
    if (num + 1 > 999) {
      common.alert.call(that, "超过最大供应量");
    } else {
      num += 1;
      that.setData({
        number: num,
      })
    }
  },
  minusNum: function () {
    var that = this,
      num = that.data.number;
    if (num - 1 < 1) {
      common.alert.call(that, "购买份数最少为1");
    } else {
      num -= 1;
      that.setData({
        number: num,
      })
    }
  },
  /**
    * 组建购物车信息
    */
  bulidShopCarInfo: function () {
    // 加入购物车
    var shopCarMap = {};
       shopCarMap.number = this.data.number,
       shopCarMap.foodAttribute = this.data.haveCheckedProps,
       shopCarMap.foodWeight = this.data.haveCheckedProp,
       shopCarMap.foodName = this.data.foodsAll.foodName,
       shopCarMap.foodprice = this.data.foodsAll.foodprice;
       shopCarMap.foodImage = this.data.foodsAll.foodImage;
       shopCarMap.foodId =this.data.foodsAll.foodId;
    // tagline = that.data.item.tagline,
    //   price = that.data.item.sellPrice,
    // shopCarMap.goodsId = this.data.goodsDetail.basicInfo.id;
    // shopCarMap.pic = this.data.goodsDetail.basicInfo.pic;
    // shopCarMap.name = this.data.goodsDetail.basicInfo.name;
    // // shopCarMap.label=this.data.goodsDetail.basicInfo.id; 规格尺寸 
    // shopCarMap.propertyChildIds = this.data.propertyChildIds;
    // shopCarMap.label = this.data.propertyChildNames;
    // shopCarMap.price = this.data.selectSizePrice;
    shopCarMap.left = "";
    shopCarMap.selected = true;
    // shopCarMap.number = this.data.buyNumber;
    // shopCarMap.logisticsType = this.data.goodsDetail.basicInfo.logisticsId;
    // shopCarMap.logistics = this.data.goodsDetail.logistics;
    // shopCarMap.weight = this.data.goodsDetail.basicInfo.weight;

    // var shopCarInfo = this.data.shopCarInfo;
    // if (!shopCarInfo.number) {
    //   shopCarInfo.number = 0;
    // }
    // if (!shopCarInfo.shopList) {
    //   shopCarInfo.shopList = [];
    // }
    // var hasSameGoodsIndex = -1;
    // for (var i = 0; i < shopCarInfo.shopList.length; i++) {
    //   var tmpShopCarMap = shopCarInfo.shopList[i];
    //   if (tmpShopCarMap.goodsId == shopCarMap.goodsId && tmpShopCarMap.propertyChildIds == shopCarMap.propertyChildIds) {
    //     hasSameGoodsIndex = i;
    //     shopCarMap.number = shopCarMap.number + tmpShopCarMap.number;
    //     break;
    //   }
    // }

    // shopCarInfo.shopNum = shopCarInfo.shopNum + this.data.buyNumber;
    // if (hasSameGoodsIndex > -1) {
    //   shopCarInfo.shopList.splice(hasSameGoodsIndex, 1, shopCarMap);
    // } else {
    //   shopCarInfo.shopList.push(shopCarMap);
    // }
    // shopCarInfo.kjId = this.data.kjId;
    return shopCarMap;
  },

  
})

