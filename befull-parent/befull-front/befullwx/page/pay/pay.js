// Page({
//   /**
//   * 页面的初始数据
//   */
//   data: {
//     timer: '',//定时器名字
//     countDownNum: '10'//倒计时初始值
//   },
//   onShow: function () {
//     //什么时候触发倒计时，就在什么地方调用这个函数
//     this.countDown();
//   },
//   countDown: function () {
//     let that = this;
//     let countDownNum = that.data.countDownNum;//获取倒计时初始值
//     //如果将定时器设置在外面，那么用户就看不到countDownNum的数值动态变化，所以要把定时器存进data里面
//     that.setData({
//       timer: setInterval(function () {//这里把setInterval赋值给变量名为timer的变量
//         //每隔一秒countDownNum就减一，实现同步
//         countDownNum--;
//         //然后把countDownNum存进data，好让用户知道时间在倒计着
//         that.setData({
//           countDownNum: countDownNum
//         })
//         //在倒计时还未到0时，这中间可以做其他的事情，按项目需求来
//         if (countDownNum == 0) {
//           //这里特别要注意，计时器是始终一直在走的，如果你的时间为0，那么就要关掉定时器！不然相当耗性能
         
//           //因为timer是存在data里面的，所以在关掉时，也要在data里取出后再关闭
//           clearInterval(that.data.timer);
//           wx.navigateTo({
//             url: '../food/food',
//           })
//           //关闭定时器之后，可作其他处理codes go here
//         }
//       }, 1000)
//     })
//   }
// })
//index.js
//从从60到到0倒计时
// function countdown(that) {
//   var second = that.data.second
//   if (second == 0) {
//     // that.setData({
//     //   second: "倒计时结束,准备返回首页"
//     // });
//     // wx.navigateTo({
//     //   url: '../food/food',
//     // })
//     return;
//   }
  
//   var time = setTimeout(function () {
//     that.setData({
//       second: second - 1
//     });
//     countdown(that);
//   }
//     , 1000)
// }
// Page({
//   data: {
//     second: 3,
//     boole: true
//   },
//   onLoad: function () {
//     countdown(this);
//   }
// });

Page({
  data: {
    showModalStatus: false,
    hidden:true,
    second: 1,
  },
  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
  },
  goBack:function(){
    wx.switchTab({
      url: "../../page/shop/shop",
    })
  },
  //弹窗
  showDialogBtn: function () { 
    var second = this.data.second
      this.setData({
        //1.实现加载窗口，并开始计时
        hidden: !this.data.hidden
      })
      //调用计时方法，如果在onLoad方法里面，一刷新界面就会计时，而不会按下按钮计时，要注意
      countdown(this);
  },
  
  //加载窗口
  changeHidden: function () {
    this.setData({
      hidden: !this.data.hidden
    });
  },
  //重载
    onLoad: function () {
    
  },

  util: function (currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({
      duration: 200,  //动画时长  
      timingFunction: "linear", //线性  
      delay: 0  //0则不延迟  
    });

    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;

    // 第3步：执行第一组动画  
    animation.opacity(0).rotateX(-100).step();

    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function () {
      // 执行第二组动画  
      animation.opacity(1).rotateX(0).step();
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({
        animationData: animation
      })

      //关闭  
      if (currentStatu == "close") {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)

    // 显示  
    if (currentStatu == "open") {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
  }
})

//执行倒计时方法
function countdown(that) {
  var second = that.data.second
  var hidden = that.data.hidden
  var showModalStatus = that.data.showModalStatus
  console.log(second +""+ hidden+""+ showModalStatus)
  if (second == 0) {
    that.setData({
      //隐藏加载项
      hidden:!that.data.hidden,
      //弹出消息框
      showModalStatus:true
    });
    return;
  }
  //倒计时
  var time = setTimeout(function () {
    that.setData({
      second: second - 1
    });
    countdown(that);
  }
    , 1000)
}