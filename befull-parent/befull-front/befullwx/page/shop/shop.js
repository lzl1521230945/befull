var app = getApp();
var server = require('../../utils/server');
Page({
	data: {
		foods: {
			/*1: {
				id: 1,
        foodName: '娃娃菜',
        pic: '/image/icons/山竹.png',
        monthiySales: 1014, 
        foodPrice: 2
			},
			2: {
				id: 2,
        foodName: '金针菇',
        pic: '/image/icons/柚子.png',
        monthiySales: 1029,
        foodPrice: 3
			},
			3: {
				id: 3,
				foodName: '方便面',
        pic: '/image/icons/柠檬.png',
        monthiySales: 1030,
        foodPrice: 2
			},
			4: {
				id: 4,
        foodName: '粉丝',
        pic: '/image/icons/榴莲.png',
        monthiySales: 1059,
        foodPrice: 1
			},
			5: {
				id: 5,
        foodName: '生菜',
        pic: '/image/icons/猕猴桃.png',
        monthiySales: 1029,
        foodPrice: 2
			},
			6: {
				id: 6,
        foodName: '白菜',
        pic: '/image/icons/草莓.png',
        monthiySales: 1064,
        foodPrice: 2
			},
			7: {
				id: 7,
        foodName: '杏鲍菇',
        pic: '/image/icons/荔枝.png',
        monthiySales: 814,
        foodPrice: 3
			},
			8: {
				id: 8,
        foodName: '香菇',
        pic: '/image/icons/葡萄.png',
        monthiySales: 124,
        foodPrice: 3
			},
			9: {
				id: 9,
        foodName: '猴头菇',
        pic: '/image/icons/西瓜.png',
        monthiySales: 102,
        foodPrice: 5
			},*/

		},
    
    foodType : [
      {
        "foodTypeId": 1, "foodTypeName": "加载中.." 
      }
    ],
  	
		cart: {
			count: 0,
			total: 0,
			list: {}
		},
		showCartDetail: false
	},

	onLoad: function () {
    this.getFoodType();
    this.getFood();
	},
  //事件处理函数
  toDetailsTap: function (e) {
    wx.navigateTo({
       url: '../Buy/Buy?id=' + e.currentTarget.dataset.id
    })
  },
  onShareAppMessage: function () {
    var shop_id = wx.getStorageSync('current_shop_id');
    var path = '/pages/start/start?shop_id=' + shop_id + '';
    return {
      title: "饱了吗点餐小程序",
      path: path
    }
  },
	follow: function () {
		this.setData({
			followed: !this.data.followed
		});
	},

  /**得到所有菜品类型 */
  getFoodType:function(){
    wx.request({
      url: "http://befull.mcaa.win/foodTypeSelectByshopid.action?shopid=2",
      dataType: "json",
      method: "post",
      header: {
        'content-type': "application/x-www-form-urlencoded" // 默认值
      },
      success: res => {
        this.setData({
          foodType: res.data.data,
        });
        console.log(this.data.foodType)
        // this.setData({
        //   typeSeleted: this.data.foodType[0].foodId
        // });
      }
    })
  },
  /**得到所有菜品 */
  getFood: function () {
    /*var id = e.target.dataset.id;
    var num = id;*/
    wx.request({
      url: "http://befull.mcaa.win/FoodSelectAll.action?shopid=2",
      dataType: "json",
      method: "post",
      header: {
        'content-type': "application/x-www-form-urlencoded" // 默认值2
      },
      success: res => {
        this.setData({
          foods: res.data.data,
        });
        console.log(this.data.foods) 
      }
    })
  },

	onGoodsScroll: function (e) {
		if (e.detail.scrollTop > 10 && !this.data.scrollDown) {
			this.setData({
				scrollDown: true
			});
		} else if (e.detail.scrollTop < 10 && this.data.scrollDown) {
			this.setData({
				scrollDown: false
			});
		}

		var scale = e.detail.scrollWidth / 570,
			scrollTop = e.detail.scrollTop / scale,
      typeSeleted,
			h = 0,
			len = this.data.foodType.length;
      var goodsLength = this.data.foods.length;
    	this.data.foodType.forEach(function (type, i) {
      var _h = 70 + goodsLength* (46 * 3 + 20 * 2);
			if (scrollTop >= h - 100 / scale) {
        typeSeleted = type.id;
			}
			h += _h;
		});
		// this.setData({
    //   typeSeleted: typeSeleted
		// });
	},

	tapClassify: function (e) {
	 var id = e.target.dataset.id;
   var num = id.substring(1,2);
   console.log(num);
    wx.request({
      url: 'http://befull.mcaa.win/foodsSelectByType.action?foodTypeId='+num,
      method:"post",
      success: function(res){
        foods:res.data.data;
      }
    })
		this.setData({
			classifyViewed: id
		});
		var self = this;
		setTimeout(function () {
			// self.setData({
      //   typeSeleted: id
			// });
		}, 100);
  },

 /* tapClassify: function (e) {
    //-----------------------------
    var id = "T" + e.target.dataset.id;
    this.setData({
      classifyViewed: id
    });
    var self = this;
    setTimeout(function () {
      self.setData({
        typeSeleted: id
      });
    }, 100);
  },*/

  
  //以下为自定义点击事件
  menuTap: function (event) {
    //event(系统给的一个框架)、currentTarget(当前鼠标点击的一个组件)、dataset(所有自定义数据的集合)、  .（变量名）
    var postId = event.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../Buy/Buy?id=' + postId
    })
  },
  
});


