package com.zuxia.struts2.inter.service;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.bean.FoodInfo;
import com.zuxia.struts2.inter.dao.FoodDao;
import com.zuxia.struts2.inter.dao.ShopDao;
import com.zuxia.struts2.inter.entity.FooddetailsinfoEntity;
import com.zuxia.struts2.inter.entity.FoodinfoEntity;
import com.zuxia.struts2.inter.util.DaoUtil;
import com.zuxia.struts2.inter.util.ServiceUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FoodService extends ServiceUtil{

    /**
     * 根据商铺id查询所有菜品信息
     * 需要传入一个商铺id
     * 2018年7月16日
     * @return
     */
    public  ArrayList<FoodInfo> queryFoodListByshopId(int shopid) throws MyException {
        int a = checkNull(shopid, "商铺id不能为空");
        ShopDao shopDao = new ShopDao();
        ArrayList<FoodInfo> foodInfos=shopDao.selectFoodDetailListbyShopId(a);
        return (ArrayList<FoodInfo>) checkNull(foodInfos,"查询菜品信息失败");
    }

    /**
     * 根据菜品id查询菜品详细信息
     * 需要传入一个菜品id
     * 2018年7月17日
     * @param foodId
     * @return
     */
    public FoodInfo queryFoodInfoByid(int foodId) throws MyException {
        int a=checkNull(foodId,"菜品名称不能为空");
        FoodDao foodDao = new FoodDao();
        FoodInfo foodInfo = foodDao.selectFoodDetailbyId(a);
        return (FoodInfo) checkNull(foodInfo,"根据菜品名称查询失败");
    }



    /**
     * 添加菜品信息以及菜品详细信息
     * 需要传入一个FooInfo对象
     * 无返回值
     * 2018年7月19日
     * @param foodInfo
     * @throws MyException
     */
    public boolean addFoodAndDetailByid(FoodInfo foodInfo) throws MyException {
        //检查图片是图片
        checkImg(foodInfo.getImgFile().getFileContentType());
        //封装菜品‘图片名.后缀’，销量为0
        foodInfo.setFoodImage(foodInfo.getImgFile().getFileFileName());
        foodInfo.setConsumptionQuantity(0);
        //上传菜品到数据库
        FoodDao foodDao = new FoodDao();
        foodDao.insertFoodDetailsInfo(foodInfo);
        //添加成功后可将图片上传到服务器
        String url="c:/img/foodImg/"+foodInfo.getShopId();
        savePicToServer(url,foodInfo.getImgFile());
        return true;
    }

    /**
     * 修改菜品信息
     * 需要传入一个修改后的菜品信息对象
     * 修改成功返回true，修改失败返回false
     * 2018年7月19号
     * @param foodinfoEntity
     * @return
     * @throws MyException
     */
    public boolean updateFoodInfoByid(FoodinfoEntity foodinfoEntity) throws MyException {
        try {
            checkNull(foodinfoEntity, "修改后的对象不能为空");
            FoodDao foodDao = new FoodDao();
            foodDao.updateFoodById(foodinfoEntity);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 修改菜品详情信息
     * 需要传入一个修改后的菜品详情信息
     * 修改成功返回true，修改失败返回false
     * 2018年7月19号
     * @param fooddetailsinfoEntity
     * @return
     * @throws MyException
     */
    public boolean updateFoodDetailsinfoByid(FooddetailsinfoEntity fooddetailsinfoEntity) throws MyException {
        try {
            checkNull(fooddetailsinfoEntity, "修改后的详情对象不能为空");
            FoodDao foodDao = new FoodDao();
            foodDao.updateDetailByid(fooddetailsinfoEntity);
            return true;
        }catch (Exception e){
            e.printStackTrace();

            return false;
        }
    }


    /**
     * 同时修改菜品基本表，菜品详细表
     *
     * @param foodInfo
     * @throws MyException
     */
    public void updateFoodAndDetailInfo(FoodInfo foodInfo) throws MyException {
        FoodDao foodDao = new FoodDao();
        /*
        * 情况1，如果图片文件为空则用户没有修改图片
        * 情况2, 如果有上传图片文件则,说明用户想要修改图片 则先吧数据插入数据库，再把图片上传到服务器
        * */
        if (foodInfo.getImgFile()!=null){
            foodInfo.setFoodImage(foodInfo.getImgFile().getFileFileName());
            checkImg(foodInfo.getImgFile().getFileContentType());
        }
        //修改数据库
        foodDao.updateFoodAndDetailByid(foodInfo);
        String url="c:/img/foodImg/"+foodInfo.getShopId();
        savePicToServer(url,foodInfo.getImgFile());
    }

    /**
     * 删除菜品及详细信息
     * 需要传入一个要删除的菜品id
     * 删除成功返回TRUE，删除失败发函FALSE
     * 2018年7月19日
     * @param fooid
     * @return
     * @throws MyException
     */
    public  boolean deleteFoodAndDetailByid(int fooid) throws MyException {
        try {
            int fooid1 = checkNull(fooid, "要删除的菜品id都不能为空");
            FoodDao foodDao = new FoodDao();
            foodDao.deleteFoodDetailsById(fooid1);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
