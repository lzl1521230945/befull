package com.zuxia.struts2.base.exception;

public class MyException extends Exception {
    private  String errorMsg;

    public MyException(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }


}
