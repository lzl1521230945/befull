package com.zuxia.struts2.inter.action;

import com.zuxia.struts2.base.action.BaseAction;
import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.bean.FileInfo;
import com.zuxia.struts2.inter.bean.FoodInfo;
import com.zuxia.struts2.inter.service.EvaluateinfoService;
import com.zuxia.struts2.inter.service.FoodService;
import com.zuxia.struts2.inter.service.FoodTypeService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.File;

/**
 * 菜品相关的action请求
 *
 */
public class FoodAction extends BaseAction {

    /**
     * 查询某商铺的所有菜品，返回所有菜品json
     * 需要传入商铺id 【shopid】
     * @return
     */
    @Action(value = "FoodSelectAll")
    public  String FoodSelectAll(){
        FoodService foodService = new FoodService();
        try {
            setSuccesResult(foodService.queryFoodListByshopId(shopid));
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return  "json";
    }

    /**
     * 查询该菜品类型下的所有 菜品，名字，月销量，价格，图片
     * 需要传入菜品类型id：【foodTypeId】
     */
    @Action(value = "foodsSelectByType")
    public String foodSelectByType(){
        FoodTypeService foodTypeService = new FoodTypeService();

        try {
            setSuccesResult(foodTypeService.queryFoodListByTypeId(foodTypeId));
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return  "json";
    }

    /**
     * 通过菜品id查该菜品的名字，销量，价格，规格，图片
     * 需要传入菜品id：【foodId】
     */
    @Action(value = "foodSelectById")
    public  String foodSelectById(){
        FoodService foodService = new FoodService();
        try {
            setSuccesResult(foodService.queryFoodInfoByid(foodId));
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return  "json";
    }



    /**
     *通过foodid查该食物的评价
     * 【foodId】
     * @return
     */
    @Action(value = "selectEvaluateByFoodid")
    public String selectEvaluateByFoodid(){
        EvaluateinfoService evaluateinfoService = new EvaluateinfoService();
        try {
            setSuccesResult(evaluateinfoService.selectEvaluateById(foodId));
        } catch (MyException e) {
            setFailResult(e.getErrorMsg());
        }
        return  "json";
    }

    /**
     * 添加菜品方法
     * 需要传入商铺id，传入菜品信息【
     * foodInfo.foodName
     * foodInfo.foodTypeId
     * foodInfo.shopId
     * foodInfo.foodprice
     * foodInfo.foodAttribute
     * foodInfo.foodWeight
     * imgFile
     * 】
     * @return
     */
    @Action(value = "foodAdd",results = {
            @Result(name = "true",type = "redirect" ,location = "http://befull.mcaa.win/index.html"),
            @Result(name = "false",type = "redirect" ,location = "http://befull.mcaa.win/index.html")
    })
//    @Action(value = "foodAdd")
    public String foodAdd(){
        FoodService foodService = new FoodService();
        //封装菜品图片信息，总价
        foodInfo.setImgFile(new FileInfo(imgFile, imgFileContentType, imgFileFileName));
        try {
            foodService.addFoodAndDetailByid(foodInfo);
            setSuccesResult("添加菜品成功");
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
            return "false";
        }
        return  "true";
    }

    /**
     * 修改菜品详细信息
     * 需要传入菜品实体，包括了需要修改的信息【
     * foodInfo.foodId
     * foodInfo.foodName
     * foodInfo.foodTypeId
     * foodInfo.shopId
     * imgFile
     * foodInfo.foodImage
     * foodInfo.foodprice
     * foodInfo.consumptionQuantity
     * foodInfo.foodAttribute
     * foodInfo.foodWeight
     * 】
     * @return
     */
    @Action(value = "foodUpdate")
    public String foodUpdate(){
        //封装图片信息
        if (imgFile!=null){
            foodInfo.setImgFile(new FileInfo(imgFile, imgFileContentType, imgFileFileName));
        }
        FoodService foodService = new FoodService();
        try {
            foodService.updateFoodAndDetailInfo(foodInfo);
            setSuccesResult("修改成功");
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return  "json";
    }

    /**
     * 删除菜品方法
     * 需要传入菜品id【foodId】
     * @return
     */
    @Action(value = "foodDelete")
    public String foodDelete(){
        FoodService foodService = new FoodService();
        try {
            foodService.deleteFoodAndDetailByid(foodId);
            setSuccesResult("删除成功");
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return  "json";
    }




    private int shopid;
    private int foodTypeId;
    private int foodId;
    private String foodName;
    private FoodInfo foodInfo;
    //上传文件的基本参数
    private File imgFile;
    private String imgFileContentType;
    private String imgFileFileName;


    public File getImgFile() {
        return imgFile;
    }

    public void setImgFile(File imgFile) {
        this.imgFile = imgFile;
    }

    public String getImgFileContentType() {
        return imgFileContentType;
    }

    public void setImgFileContentType(String imgFileContentType) {
        this.imgFileContentType = imgFileContentType;
    }

    public String getImgFileFileName() {
        return imgFileFileName;
    }

    public void setImgFileFileName(String imgFileFileName) {
        this.imgFileFileName = imgFileFileName;
    }

    public FoodInfo getFoodInfo() {
        return foodInfo;
    }

    public void setFoodInfo(FoodInfo foodInfo) {
        this.foodInfo = foodInfo;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getShopid() {
        return shopid;
    }

    public void setShopid(int shopid) {
        this.shopid = shopid;
    }

    public int getFoodTypeId() {
        return foodTypeId;
    }

    public void setFoodTypeId(int foodTypeId) {
        this.foodTypeId = foodTypeId;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }
}