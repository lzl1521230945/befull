package com.zuxia.struts2.inter.service;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.bean.FoodTypeInfo;
import com.zuxia.struts2.inter.dao.FoodTypeDao;
import com.zuxia.struts2.inter.entity.FoodinfoEntity;
import com.zuxia.struts2.inter.entity.FoodtypeinfoEntity;
import com.zuxia.struts2.inter.util.ServiceUtil;


import java.util.ArrayList;
import java.util.List;

public class FoodTypeService extends ServiceUtil{
    /**
     * 根据商铺id查询菜品类型
     * 需要传入一个商铺id
     * 2018年7月16
     * @param shopid
     * @return
     */
    public ArrayList<FoodTypeInfo> queryFoodTypeListByShopid(int shopid) throws MyException {
        int a = checkNull(shopid,"商铺id不能为空");
        FoodTypeDao foodTypeDao = new FoodTypeDao();
        ArrayList<FoodtypeinfoEntity> list = foodTypeDao.selectFoodTypeListByShopId(a);
        ArrayList<FoodTypeInfo> foodTypeInfos = new ArrayList<FoodTypeInfo>();


        //吧每个菜品类型下，相应的菜品id的集合,封装上去
        for (int i = 0; i <list.size() ; i++) {
            //先取出菜品类型id，再去查相应的菜品集合
            FoodtypeinfoEntity foodtypeinfoEntity = list.get(i);
            ArrayList<FoodinfoEntity> foodInfoList= foodTypeDao.selectFoodlistByTypeId(foodtypeinfoEntity.getFoodTypeId());

            //把查出来的菜品集合的id取出来重新装一个数组
            ArrayList<Integer> foodIdsOfType=new ArrayList<Integer>();
            for (int j = 0; j <foodInfoList.size() ; j++) {
                foodIdsOfType.add((foodInfoList.get(j)).getFoodId());
            }
            FoodTypeInfo foodTypeInfo = new FoodTypeInfo(foodtypeinfoEntity.getFoodTypeId(),
                    foodtypeinfoEntity.getFoodTypeName(),
                    foodtypeinfoEntity.getShopId(),
                    foodIdsOfType
                    );
            foodTypeInfos.add(foodTypeInfo);
        }

        return  (ArrayList<FoodTypeInfo>)checkNull( foodTypeInfos,"菜品类型查询失败");
    }

    /**
     * 根据菜品类型查询菜品
     * 需要传入一个菜品类型id
     * 2018年7月17日
     * @param foodTypeId
     * @return
     */
    public List queryFoodListByTypeId(int foodTypeId) throws MyException {
         int a = checkNull(foodTypeId,"菜品类型id不能为空");
        FoodTypeDao foodTypeDao = new FoodTypeDao();
        List fodinfo = foodTypeDao.selectFoodlistByTypeId(a);
        return (List)checkNull(fodinfo,"菜品类型为空");

    }

    /**
     * 添加食品类型方法
     * @param foodTypename
     * @throws MyException
     */
    public void insertFoodType(String foodTypename) throws MyException {
        checkNull(foodTypename,"类型名不能为空");
        FoodTypeDao foodTypeDao=new FoodTypeDao();
        foodTypeDao.insertFoodType(foodTypename);
    }

    /**
     * 删除菜类型
     * @param foodTypeId
     * @throws MyException
     */
    public  void deleteFoodType(int foodTypeId) throws MyException {
        checkNull(foodTypeId,"要删除的类型id不能为空");
        FoodTypeDao foodTypeDao = new FoodTypeDao();
        foodTypeDao.deleteFoodTypeById(foodTypeId);
    }

    /**
     * 修改菜品类型
     * @param foodtypeinfoEntity
     * @throws MyException
     */
    public void updateFoodType( FoodtypeinfoEntity foodtypeinfoEntity) throws MyException {
        checkNull(foodtypeinfoEntity.getFoodTypeId(),"要修改的id不能为空");
        checkNull(foodtypeinfoEntity.getFoodTypeName(),"要修改的类型名不能为空");
        FoodTypeDao foodTypeDao = new FoodTypeDao();
        foodTypeDao.updateFoodTypeInfo(foodtypeinfoEntity);
    }






}
