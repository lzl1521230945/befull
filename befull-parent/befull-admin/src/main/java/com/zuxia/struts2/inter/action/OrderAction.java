package com.zuxia.struts2.inter.action;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zuxia.struts2.base.action.BaseAction;
import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.bean.OrderDetailInfo;
import com.zuxia.struts2.inter.bean.OrderInfo;
import com.zuxia.struts2.inter.service.OrderService;
import org.apache.struts2.convention.annotation.Action;

import java.util.ArrayList;
import java.util.List;

public class OrderAction extends BaseAction {

    /**
     * 添加订单方法
     * 【
     *  orderInfo.orderRemarks
     *  orderInfo.seatId
     *  orderInfo.shopId
     *  foodName
     *  foodNumber
     *  foodAttribute
     *  foodWeight
     *  price
     *  totalPrice
     * 】
     *
     * @return
     */
    @Action(value = "addOrder")
    public String addOrder(){
        OrderService orderService=new OrderService();

        OrderDetailInfo orderDetailInfo = new OrderDetailInfo(foodName,foodNumber,foodAttribute,foodWeight,price,foodNumber*price);

        ArrayList<OrderDetailInfo> list= new ArrayList<OrderDetailInfo>();

        list.add(orderDetailInfo);

        try {
            orderInfo.setOrderDetailInfoList(list);
            orderService.insertOrder(orderInfo);
            setSuccesResult("添加成功");
        } catch (MyException e) {
            setFailResult(e.getErrorMsg());
        }
        return  "json";
    }



    private OrderInfo orderInfo;
    private String foodName;
    private int foodNumber;
    private String foodAttribute;
    private String foodWeight;
    private int price;
    private int totalPrice;

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getFoodNumber() {
        return foodNumber;
    }

    public void setFoodNumber(int foodNumber) {
        this.foodNumber = foodNumber;
    }

    public String getFoodAttribute() {
        return foodAttribute;
    }

    public void setFoodAttribute(String foodAttribute) {
        this.foodAttribute = foodAttribute;
    }

    public String getFoodWeight() {
        return foodWeight;
    }

    public void setFoodWeight(String foodWeight) {
        this.foodWeight = foodWeight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }
}
