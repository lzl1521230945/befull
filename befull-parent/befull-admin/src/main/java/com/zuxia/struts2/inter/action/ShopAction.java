package com.zuxia.struts2.inter.action;

import com.opensymphony.xwork2.ActionSupport;
import com.zuxia.struts2.base.action.BaseAction;
import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.entity.AdmininfoEntity;
import com.zuxia.struts2.inter.entity.CookinfoEntity;
import com.zuxia.struts2.inter.service.CookService;
import com.zuxia.struts2.inter.service.ShopService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

public class ShopAction extends BaseAction {
    /*
     * 通过id查店铺信息
     * 【 shopid】*/
    @Action("shopSelectById")
    public String shopSelectById(){
        ShopService shopService = new ShopService();
        try {
            setSuccesResult(shopService.selectShopInfoByid(shopid));
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return "json";
    }

    /**
     * 厨师登陆方法【
     *  userName
     *  password
     *
     * 】
     * @return
     */
    @Action(value = "cookLogin",results = {
            @Result(name = "true",type = "redirect" ,location = "http://befull.mcaa.win/index.html"),
            @Result(name = "false",type = "redirect" ,location = "http://befull.mcaa.win")
    })
//    @Action(value = "cookLogin")
    public String cookLogin(){
        CookinfoEntity cookinfoEntity = new CookinfoEntity();
        cookinfoEntity.setCookName(userName);
        cookinfoEntity.setCookPassword(password);

        CookService cookService=new CookService();
        try {
            cookService.cookLogin(cookinfoEntity);
            setSuccesResult("登陆成功");
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
            return "false";
        }
        return "true";
    }









    private int shopid;
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getShopid() {
        return shopid;
    }

    public void setShopid(int shopid) {
        this.shopid = shopid;
    }
}
