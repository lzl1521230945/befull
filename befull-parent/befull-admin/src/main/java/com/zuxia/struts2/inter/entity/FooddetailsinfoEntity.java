package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "fooddetailsinfo", schema = "ordersystemdb", catalog = "")
public class FooddetailsinfoEntity {
    private int foodDetailsId;
    private String foodImage;
    private int foodprice;
    private Integer consumptionQuantity;
    private Integer foodId;
    private String foodAttribute;
    private String foodWeight;

    public FooddetailsinfoEntity() {
    }

    public FooddetailsinfoEntity(String foodImage, int foodprice, Integer consumptionQuantity,
                                 Integer foodId, String foodAttribute, String foodWeight) {
        this.foodImage = foodImage;
        this.foodprice = foodprice;
        this.consumptionQuantity = consumptionQuantity;
        this.foodId = foodId;
        this.foodAttribute = foodAttribute;
        this.foodWeight = foodWeight;
    }

    @Id
    @Column(name = "foodDetailsId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getFoodDetailsId() {
        return foodDetailsId;
    }

    public void setFoodDetailsId(int foodDetailsId) {
        this.foodDetailsId = foodDetailsId;
    }

    @Basic
    @Column(name = "foodImage")
    public String getFoodImage() {
        return foodImage;
    }

    public void setFoodImage(String foodImage) {
        this.foodImage = foodImage;
    }

    @Basic
    @Column(name = "foodprice")
    public int getFoodprice() {
        return foodprice;
    }

    public void setFoodprice(int foodprice) {
        this.foodprice = foodprice;
    }

    @Basic
    @Column(name = "consumptionQuantity")
    public Integer getConsumptionQuantity() {
        return consumptionQuantity;
    }

    public void setConsumptionQuantity(Integer consumptionQuantity) {
        this.consumptionQuantity = consumptionQuantity;
    }

    @Basic
    @Column(name = "foodId")
    public Integer getFoodId() {
        return foodId;
    }

    public void setFoodId(Integer foodId) {
        this.foodId = foodId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FooddetailsinfoEntity that = (FooddetailsinfoEntity) o;
        return foodDetailsId == that.foodDetailsId &&
                foodprice == that.foodprice &&
                Objects.equals(foodImage, that.foodImage) &&
                Objects.equals(consumptionQuantity, that.consumptionQuantity) &&
                Objects.equals(foodId, that.foodId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(foodDetailsId, foodImage, foodprice, consumptionQuantity, foodId);
    }

    @Basic
    @Column(name = "foodAttribute")
    public String getFoodAttribute() {
        return foodAttribute;
    }

    public void setFoodAttribute(String foodAttribute) {
        this.foodAttribute = foodAttribute;
    }

    @Basic
    @Column(name = "foodWeight")
    public String getFoodWeight() {
        return foodWeight;
    }
    
    public void setFoodWeight(String foodWeight) {
        this.foodWeight = foodWeight;
    }
}
