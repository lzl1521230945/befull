package com.zuxia.struts2.inter.bean;

import com.zuxia.struts2.inter.entity.OrderdetailsinfoEntity;

public class OrderDetailInfo  {
    private int orderId;
    private String foodName;       //食物名称
    private int foodNumber;        //食物数量
    private String foodAttribute;   //食物属性
    private String foodWeight;      //食物分量
    private int price;              //价格单价
    private int totalPrice;        //总价


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getFoodNumber() {
        return foodNumber;
    }

    public void setFoodNumber(int foodNumber) {
        this.foodNumber = foodNumber;
    }

    public String getFoodAttribute() {
        return foodAttribute;
    }

    public void setFoodAttribute(String foodAttribute) {
        this.foodAttribute = foodAttribute;
    }

    public String getFoodWeight() {
        return foodWeight;
    }

    public void setFoodWeight(String foodWeight) {
        this.foodWeight = foodWeight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }


    public OrderDetailInfo() {
    }

    public OrderDetailInfo(String foodName, int foodNumber, String foodAttribute, String foodWeight, int price, int totalPrice) {
        this.foodName = foodName;
        this.foodNumber = foodNumber;
        this.foodAttribute = foodAttribute;
        this.foodWeight = foodWeight;
        this.price = price;
        this.totalPrice = totalPrice;
    }
}
