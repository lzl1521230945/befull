package com.zuxia.struts2.base.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;


public class HibernateUtils {
    private static SessionFactory factory = null;
    static{
        Configuration configure = new Configuration().configure();
        factory = configure.buildSessionFactory();
    }
    /**
     * 用于获取session
     * @return
     */
    public static Session getSession(){
        // session不是线程安全对象，每一次操作时建议重新获取一个session对象
        Session session = factory.openSession();
        return session;
    }

    /**
     * 根据hql语句获取query对象
     * @param hql
     * @return
     */
    public static Query getQuery(String hql){
        Session session = getSession();
        Query query = session.createQuery(hql);
        return query;
    }
}
