package com.zuxia.struts2.inter.util;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.base.util.HibernateUtils;
import com.zuxia.struts2.inter.bean.FoodInfo;
import com.zuxia.struts2.inter.bean.OrderInfo;
import com.zuxia.struts2.inter.entity.FooddetailsinfoEntity;
import com.zuxia.struts2.inter.entity.FoodinfoEntity;
import com.zuxia.struts2.inter.entity.OrderdetailsinfoEntity;
import com.zuxia.struts2.inter.entity.OrderinfoEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoUtil {
    static Session session = null;
    static Transaction transaction = null;

    /**
     * 封装菜品方法
     * 需要传入一个obj的值
     *
     * @param obj
     * @return
     */
    public static FoodInfo getTogether(Object obj) {
        //此obj是一个数组，但不能取出数据来，我们把他强转为一个object的一个数组，再用这个数组来取值
        Object[] objlist = (Object[]) obj;

        //吧这个数组内的实体取出来，转为想要的类型
        FoodinfoEntity food = (FoodinfoEntity) objlist[0];
        FooddetailsinfoEntity foodDetail = (FooddetailsinfoEntity) objlist[1];

        String imgScr="http://befull.mcaa.win/foodImg/"+food.getShopId()+"/"+foodDetail.getFoodImage();

        //封装数据
        FoodInfo foodInfo = new FoodInfo(food.getFoodId(),
                food.getFoodName(),
                food.getFoodTypeId(),
                food.getShopId(),
                imgScr,
                foodDetail.getFoodprice(),
                foodDetail.getConsumptionQuantity(),
                foodDetail.getFoodAttribute(),
                foodDetail.getFoodWeight(),
                foodDetail.getFoodAttribute().split(","),
                foodDetail.getFoodWeight().split(",")

        );
        return foodInfo;
    }
    /*
    * 封装订单方法
    * 传要传入obj
    * */
//    public static OrderInfo getOrderTogether(Object obj){
//        //此obj是一个数组，但不能取出数据来，我们把他强转为一个object的一个数组，再用这个数组来取值
//        Object[] objlist = (Object[]) obj;
//
//        //吧这个数组内的实体取出来，转为想要的类型
//        OrderinfoEntity order = (OrderinfoEntity) objlist[0];
//        OrderdetailsinfoEntity orderdetails = (OrderdetailsinfoEntity) objlist[1];
//
//        //封装数据
//        OrderInfo orderInfo = new OrderInfo(
//                order.getOrderId(),
//                order.getOrderRemarks(),
//                order.getSeatId(),
//                order.getShopId(),
//                order.getOrderTime(),
//                order.geto
//
//
//                );
//        return orderInfo;
//    }


/*
* 修改数据库公用方法
* */
    public static void dataUpdate(Object obj) throws MyException {
        try {
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();
            //执行修改
            session.update(obj);
            //提交事务
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();

        }
    }

//
//    public  static  String encryption(String obj,String salt){
//
//    }

}
