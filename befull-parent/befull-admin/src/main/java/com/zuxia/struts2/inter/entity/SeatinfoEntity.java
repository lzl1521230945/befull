package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "seatinfo", schema = "ordersystemdb", catalog = "")
public class SeatinfoEntity {
    private int seatId;
    private String seatName;
    private Integer shopId;

    @Id
    @Column(name = "seatId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getSeatId() {
        return seatId;
    }

    public void setSeatId(int seatId) {
        this.seatId = seatId;
    }

    @Basic
    @Column(name = "seatName")
    public String getSeatName() {
        return seatName;
    }

    public void setSeatName(String seatName) {
        this.seatName = seatName;
    }

    @Basic
    @Column(name = "shopId")
    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SeatinfoEntity that = (SeatinfoEntity) o;
        return seatId == that.seatId &&
                Objects.equals(seatName, that.seatName) &&
                Objects.equals(shopId, that.shopId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(seatId, seatName, shopId);
    }
}
