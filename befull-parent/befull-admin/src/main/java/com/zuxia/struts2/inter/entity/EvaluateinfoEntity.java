package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "evaluateinfo", schema = "ordersystemdb", catalog = "")
public class EvaluateinfoEntity {
    private int evaluateId;
    private String userEvaluate;
    private Integer foodId;

    public EvaluateinfoEntity() {
    }

    public EvaluateinfoEntity(String userEvaluate, Integer foodId) {
        this.userEvaluate = userEvaluate;
        this.foodId = foodId;
    }

    @Id
    @Column(name = "evaluateId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getEvaluateId() {
        return evaluateId;
    }

    public void setEvaluateId(int evaluateId) {
        this.evaluateId = evaluateId;
    }

    @Basic
    @Column(name = "userEvaluate")
    public String getUserEvaluate() {
        return userEvaluate;
    }

    public void setUserEvaluate(String userEvaluate) {
        this.userEvaluate = userEvaluate;
    }

    @Basic
    @Column(name = "foodId")
    public Integer getFoodId() {
        return foodId;
    }

    public void setFoodId(Integer foodId) {
        this.foodId = foodId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvaluateinfoEntity that = (EvaluateinfoEntity) o;
        return evaluateId == that.evaluateId &&
                Objects.equals(userEvaluate, that.userEvaluate) &&
                Objects.equals(foodId, that.foodId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(evaluateId, userEvaluate, foodId);
    }
}
