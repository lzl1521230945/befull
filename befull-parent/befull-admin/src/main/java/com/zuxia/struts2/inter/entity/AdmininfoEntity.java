package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "admininfo", schema = "ordersystemdb", catalog = "")
public class AdmininfoEntity {
    private int adminId;
    private String adminName;
    private String adminPassword;
    private String salt;

    @Id
    @Column(name = "adminId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    @Basic
    @Column(name = "adminName")
    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    @Basic
    @Column(name = "adminPassword")
    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdmininfoEntity that = (AdmininfoEntity) o;
        return adminId == that.adminId &&
                adminPassword == that.adminPassword &&
                Objects.equals(adminName, that.adminName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(adminId, adminName, adminPassword);
    }

    @Basic
    @Column(name = "salt")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
