package com.zuxia.struts2.inter.service;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.dao.FoodDao;
import com.zuxia.struts2.inter.entity.EvaluateinfoEntity;
import com.zuxia.struts2.inter.util.ServiceUtil;

import java.util.ArrayList;
import java.util.List;

public class EvaluateinfoService extends ServiceUtil {
    /**
     * 根据菜品id查询评价信息
     * 需要传入一个菜品id
     * 返回一个list集合
     *2018年7月18号
     * @param Foodid
     * @return
     * @throws MyException
     */
    public ArrayList<EvaluateinfoEntity> selectEvaluateById(int Foodid) throws MyException {
        int a = checkNull(Foodid, "菜品id不能为空");
        FoodDao foodDao = new FoodDao();
        return (ArrayList<EvaluateinfoEntity>) checkNull(
                foodDao.selectEvaluateById(a),"查询结果为空");
    }

    /**
     * 根据菜品id添加菜品评价
     * 需要传入一个菜品id和评价类容
     * 2018年7月19日
     * @throws MyException
     */
    public void addEvaluateInfoByid(EvaluateinfoEntity evaluateinfoEntity) throws MyException {
        checkNull(evaluateinfoEntity.getFoodId(), "菜品id不能为空");
        checkNull(evaluateinfoEntity.getUserEvaluate(), "评价类容不能为空");
        FoodDao foodDao = new FoodDao();
        foodDao.insertEvaluateInfoodId(evaluateinfoEntity);
    }


}
