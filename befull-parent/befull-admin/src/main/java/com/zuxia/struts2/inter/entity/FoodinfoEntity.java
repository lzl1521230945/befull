package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Objects;

@Entity
@Table(name = "foodinfo", schema = "ordersystemdb", catalog = "")
public class FoodinfoEntity {
    private int foodId;
    private String foodName;
    private Integer foodTypeId;
    private Integer shopId;


    public FoodinfoEntity() {
    }

    public FoodinfoEntity(int foodId, String foodName, Integer foodTypeId, Integer shopId) {
        this.foodId = foodId;
        this.foodName = foodName;
        this.foodTypeId = foodTypeId;
        this.shopId = shopId;
    }

    @Id
    @Column(name = "foodId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    @Basic
    @Column(name = "foodName")
    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    @Basic
    @Column(name = "foodTypeId")
    public Integer getFoodTypeId() {
        return foodTypeId;
    }

    public void setFoodTypeId(Integer foodTypeId) {
        this.foodTypeId = foodTypeId;
    }

    @Basic
    @Column(name = "shopId")
    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoodinfoEntity that = (FoodinfoEntity) o;
        return foodId == that.foodId &&
                Objects.equals(foodName, that.foodName) &&
                Objects.equals(foodTypeId, that.foodTypeId) &&
                Objects.equals(shopId, that.shopId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(foodId, foodName, foodTypeId, shopId);
    }
}
