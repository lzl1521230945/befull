package com.zuxia.struts2.inter.service;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.dao.CookDao;
import com.zuxia.struts2.inter.entity.CookinfoEntity;
import com.zuxia.struts2.inter.util.ServiceUtil;

import java.util.UUID;

public class CookService extends ServiceUtil {

    public boolean cookLogin(CookinfoEntity cookinfoEntity) throws MyException {
        checkNull(cookinfoEntity.getCookName(),"必须输入用户名");
        checkNull(cookinfoEntity.getCookPassword(),"必须输入密码");

        CookDao cookDao=new CookDao();

        cookinfoEntity.setCookPassword(
                strToCode(cookinfoEntity.getCookPassword(),cookDao.selectSalt(cookinfoEntity.getCookName())));


        return cookDao.CookLogin(cookinfoEntity);
    }
}
