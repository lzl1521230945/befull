package com.zuxia.struts2.inter.dao;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.base.util.HibernateUtils;
import com.zuxia.struts2.inter.bean.OrderDetailInfo;
import com.zuxia.struts2.inter.bean.OrderInfo;
import com.zuxia.struts2.inter.entity.OrderdetailsinfoEntity;
import com.zuxia.struts2.inter.entity.OrderinfoEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class OrderDao {
    Session session = null;
    Transaction transaction = null;

    /*
     * 添加订单信息表     (有点问题待修正)
     * 通过接受的OrderLisInfo集合数据
     * 添加到详细信息表中在把信息表
     * */
    public void insertOrderAndDetails(OrderInfo orderInfo) throws MyException {
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();
            //实例化订单表
            OrderinfoEntity orderinfoEntity = new OrderinfoEntity(
                    orderInfo.getOrderRemarks(),
                    orderInfo.getSeatId(),
                    orderInfo.getShopId(),
                    orderInfo.getOrderTime(),
                    orderInfo.getStatus()
            );
            int save = (int) session.save(orderinfoEntity);

            //循环添加订单信息详细表
            ArrayList<OrderDetailInfo> orderDetailInfoList = orderInfo.getOrderDetailInfoList();
            for (int i = 0; i <orderDetailInfoList.size() ; i++) {
                OrderDetailInfo orderDetailInfo = orderDetailInfoList.get(i);
                OrderdetailsinfoEntity orderdetailsinfoEntity = new OrderdetailsinfoEntity(
                        orderDetailInfo.getFoodName(),
                        orderDetailInfo.getFoodNumber(),
                        orderDetailInfo.getFoodAttribute(),
                        orderDetailInfo.getFoodWeight(),
                        orderDetailInfo.getPrice(),
                        orderDetailInfo.getTotalPrice(),
                        save

                );
                session.save(orderdetailsinfoEntity);
            }
            //提交事务
            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }

    }

    /*
     * 查询订单的详细信息
     * 连表查询全部订单信息
     * 1.先查基础信息，封装到orderinfo
     * 2.再查，详细菜品列表，封装到ArrlyList《sfdfdfd》  再把这个arrlylist封装到arderinfo
     * 合并后的订单信息（OrderinfoEntity-OrderdetailsinfoEntity）
     * */
    public List selectOrderDetails() throws MyException {
        List list = null;
        OrderInfo orderTogether = null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from OrderinfoEntity a left join OrderdetailsinfoEntity b " +
                    "on a.orderId=b.orderId");
            //使用query对象的list对象得到数据
            list = query.list();
//           orderTogether = DaoUtil.getOrderTogether(list);

        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }
        return list;

    }

    /*
     * 删除订单表（全部删除）
     * 根据订单Id进行删除
     * 有点bug不能删除全部的订单详细
     * */
    public void deleteOrderDetailsById(int orderId) throws MyException {
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();
            //加载要修改的数据
            OrderinfoEntity orderinfoEntity = session.get(OrderinfoEntity.class, new Integer(orderId));
            //执行删除
            session.delete(orderinfoEntity);
            //加载要修改的数据
            OrderdetailsinfoEntity orderdetailsinfoEntity = session.get(OrderdetailsinfoEntity.class, new Integer(orderId));
            //执行删除
            session.delete(orderdetailsinfoEntity);
            //提交事务
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }
    }

    /*
     * 查询订单信息表
     * 根据传递的订单Id进行查询订单信息表
     * */
    public List selectOrderInfoById(int orderId) throws MyException {
        List list = null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from OrderinfoEntity where orderId = ?");
            //填写站位符，根据id查询数据
            query.setParameter(0, orderId);
            //使用query对象的list对象得到数据
            list = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }
        return list;

    }

    /*
     * 查询订单详细信息表
     * 根据传递的订单id进行查询订单详细信息
     * */
    public List selectOrderdetailsById(int orderId) throws MyException {
        List list = null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from OrderdetailsinfoEntity where orderId = ?");
            //填写站位符，根据id查询数据
            query.setParameter(0, orderId);
            //使用query对象的list对象得到数据
            list = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }
        return list;
    }

    /*
     * 查询订单状态
     * 根据传递的订单状态查询订单状态是否为1或者0
     * 并返回查询的集合
     * */
    public List selectOrderdetailsByStatus(int status) throws MyException {
        List list = null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from OrderinfoEntity where status = ?");
            //填写站位符，根据id查询数据
            query.setParameter(0, status);
            //使用query对象的list对象得到数据
            list = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }
        return list;

    }
}
