package com.zuxia.struts2.inter.action;

import com.zuxia.struts2.base.action.BaseAction;
import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.entity.FoodtypeinfoEntity;
import com.zuxia.struts2.inter.service.FoodTypeService;
import org.apache.struts2.convention.annotation.Action;

public class FoodTypeAction extends BaseAction {

    /**
     * 查询商铺所有菜品的类型
     * 必须传入商铺id【shopid】
     */
    @Action(value = "foodTypeSelectByshopid")
    public String foodTypeSelect() {
        FoodTypeService foodTypeService = new FoodTypeService();
        try {
            setSuccesResult(foodTypeService.queryFoodTypeListByShopid(shopid));
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return "json";
    }

    /**
     * 为某店添加菜品类型
     * 需要传入【
     * foodtypeinfo.shopId
     * foodtypeinfo.foodTypeName
     * 】
     *
     * @return
     */
    @Action(value = "foodTypeAdd")
    public String foodTypeAdd() {
        FoodTypeService foodTypeService = new FoodTypeService();
        try {

            foodTypeService.insertFoodType(foodTypename);
            //传入店铺id，菜品类型名
            setSuccesResult("添加成功");
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return "json";
    }

    /**
     * 菜品类型修改功能
     * 需要传入菜品类型实体【
     * foodtypeinfo.foodTypeId
     * foodtypeinfo.foodTypeName
     * 】
     *
     * @return
     */
    @Action(value = "foodTypeUpdate")
    public String foodTypeUpdate() {
        FoodTypeService foodTypeService = new FoodTypeService();
        try {
            foodTypeService.updateFoodType(foodtypeinfo);
            setSuccesResult("修改类型成功");
        } catch (MyException e) {
            e.printStackTrace();
            setFailResult(e.getErrorMsg());
        }
        return "json";
    }

    /**
     * 菜品类型的删除
     * 需要传入菜品类型id【foodTypeId】
     *
     * @return
     */
    @Action(value = "foodTypeDelete")
    public String foodTypeDelete() {
        FoodTypeService foodTypeService = new FoodTypeService();
        try {
            foodTypeService.deleteFoodType(foodTypeId);
            setSuccesResult("删除类型成功");
        } catch (MyException e) {
            setFailResult(e.getErrorMsg());
        }
        return "json";
    }


    private int shopid;
    private int foodTypeId;
    private FoodtypeinfoEntity foodtypeinfo;
    private String foodTypename;


    public int getFoodTypeId() {
        return foodTypeId;
    }

    public void setFoodTypeId(int foodTypeId) {
        this.foodTypeId = foodTypeId;
    }

    public FoodtypeinfoEntity getFoodtypeinfo() {
        return foodtypeinfo;
    }

    public void setFoodtypeinfo(FoodtypeinfoEntity foodtypeinfo) {
        this.foodtypeinfo = foodtypeinfo;
    }

    public int getShopid() {
        return shopid;
    }

    public void setShopid(int shopid) {
        this.shopid = shopid;
    }

    public String getFoodTypename() {
        return foodTypename;
    }

    public void setFoodTypename(){
        this.foodTypename=foodTypename;
    }
}
