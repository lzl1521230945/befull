
package com.zuxia.struts2.inter.dao;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.base.util.HibernateUtils;
import com.zuxia.struts2.inter.bean.FoodInfo;
import com.zuxia.struts2.inter.entity.ShopinfoEntity;
import com.zuxia.struts2.inter.util.DaoUtil;
import com.zuxia.struts2.inter.util.ServiceUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class ShopDao {
    Session session = null;
    Transaction transaction =null;
    /*
     * 添加商铺
     * */
    public void insertShopInfo(String shopName) throws MyException {
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();
            //实例化商铺信息表
            ShopinfoEntity shopinfoEntity = new ShopinfoEntity();
            //设置值
            shopinfoEntity.setShopName(shopName);
            //持久化添加
            session.save(shopinfoEntity);
            //提交事务
            transaction.commit();
        }catch (Exception e){
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }

    }
    /*
     * 通过id查询商铺信息
     *
     * */
    public ShopinfoEntity selectShopInfoByid(int shopId) throws MyException {
        List list= null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            ////得到query对象并写入hql语句
            Query from_shopinfoEntity = session.createQuery("from ShopinfoEntity where shopId=? ");
            from_shopinfoEntity.setParameter(0,shopId);
            //使用query对象的list方法得到数据集合
            list = from_shopinfoEntity.list();

        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        return (ShopinfoEntity)list.get(0);
    }

    /*
     * 查询所有商铺信息
     *
     * */
    public ArrayList<ShopinfoEntity> selectAllShop() throws MyException {
        List list= null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            ////得到query对象并写入hql语句
            Query from_shopinfoEntity = session.createQuery("from ShopinfoEntity");
            //使用query对象的list方法得到数据集合
            list = from_shopinfoEntity.list();

        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        return (ArrayList<ShopinfoEntity>)list;
    }

    /*
     * 根据商铺id查询食物信息表及详细信息
     * 返回list集合
     * */
    public ArrayList<FoodInfo> selectFoodDetailListbyShopId(int shopId) throws MyException {
        List list = null;
        try {
            //加载公共工厂
            session = HibernateUtils.getSession();
            //得到query对象并写入hql语句
            Query from_foodinfoEntity = session.createQuery("from  FoodinfoEntity a left join FooddetailsinfoEntity" +
                    " b on a.foodId=b.foodId where shopId = ?  ");
            //填写站位符，根据id查询数据
            from_foodinfoEntity.setParameter(0,shopId);
            //使用query对象的list方法得到数据集合
            list = from_foodinfoEntity.list();

        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        ArrayList<FoodInfo> foodInfos = new ArrayList<FoodInfo>();
        for (Object obj :list) {
            foodInfos.add(DaoUtil.getTogether(obj));
        }
        return foodInfos;
    }
    /*
    * 修改商品
    * 根据传的一个修改商铺对象进行修改
    * */
    public void updateShopInfo(ShopinfoEntity shopinfoEntity) throws MyException {
        try {
             //加载工厂
            DaoUtil.dataUpdate(shopinfoEntity);
        } catch (Exception e) {
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }

    }
    /*
    * 删除商铺
    * 根据传递的删除商铺id进行删除??????????????????????????判断商铺
    * */
    public void deleteShopByid(int shopId) throws MyException {
       if (this.selectFoodDetailListbyShopId(shopId)==null){
           throw new MyException("该商铺下有数据，无法进行操作！");
       }else{
           try {
               //加载工厂
               session = HibernateUtils.getSession();
               //加载事务
               transaction = session.beginTransaction();
               //加载要修改的数据
               ShopinfoEntity shopinfoEntity = session.get(ShopinfoEntity.class, new Integer(shopId));
               //执行修改
               session.update(shopinfoEntity);
               //提交事务
               transaction.commit();
           }catch (Exception e){
               e.printStackTrace();
               //回滚事务
               transaction.rollback();
               throw new MyException("数据库异常，请联系管理员！");
           }finally {
               session.close();
           }
       }

    }

}