package com.zuxia.struts2.inter.dao;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.base.util.HibernateUtils;
import com.zuxia.struts2.inter.entity.AdmininfoEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class AdminDao {
    Session session=null;
    Transaction transaction=null;

    /*
    * 查询是否有该用户
    * 接受传递集合中的用户名字和密码
    * */
    public List selectAdminLogin(AdmininfoEntity admininfoEntity) throws MyException {
        List list = null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from AdmininfoEntity where adminName =? and adminPassword = ?");
            //填写站位符，根据id查询数据
            query.setParameter(0,admininfoEntity.getAdminName());
            query.setParameter(1,admininfoEntity.getAdminPassword());
            //使用query对象的list对象得到数据
            list = query.list();

        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        return list;
    }

//    public String selectSalt(String userName){
//        List list = null;
//        try {
//            //加载工厂
//            session = HibernateUtils.getSession();
//            // 1、得到query对象，并写入hql语句
//            Query query = session.createQuery("from AdmininfoEntity where adminName =?");
//            //填写站位符，根据id查询数据
//            query.setParameter(0,userName);
//            //使用query对象的list对象得到数据
//            list = query.list();
//
//        }catch (Exception e){
//            e.printStackTrace();
//            throw new MyException("数据库异常，请联系管理员！");
//        }finally {
//            session.close();
//        }
//        return list;
//    }
}
