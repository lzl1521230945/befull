package com.zuxia.struts2.inter.bean;

import java.io.File;

public class FileInfo {
    private File File;
    private String FileContentType;
    private String FileFileName;

    public FileInfo() {
    }

    public FileInfo(java.io.File file, String fileContentType, String fileFileName) {
        File = file;
        FileContentType = fileContentType;
        FileFileName = fileFileName;
    }

    public java.io.File getFile() {
        return File;
    }

    public void setFile(java.io.File file) {
        File = file;
    }

    public String getFileContentType() {
        return FileContentType;
    }

    public void setFileContentType(String fileContentType) {
        FileContentType = fileContentType;
    }

    public String getFileFileName() {
        return FileFileName;
    }

    public void setFileFileName(String fileFileName) {
        FileFileName = fileFileName;
    }
}
