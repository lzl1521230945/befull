package com.zuxia.struts2.inter.util;

import com.alibaba.fastjson.JSON;
import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.bean.OrderDetailInfo;
import com.zuxia.struts2.inter.bean.OrderInfo;
import com.zuxia.struts2.inter.dao.CookDao;
import com.zuxia.struts2.inter.entity.CookinfoEntity;
import com.zuxia.struts2.inter.service.CookService;
import com.zuxia.struts2.inter.util.SHAencryption;

import java.util.ArrayList;
import java.util.UUID;

public class test extends ServiceUtil {
    public static void main(String[] args) {
        loginTest();


    }

    private static void loginTest() {
        CookinfoEntity cookinfoEntity=new CookinfoEntity();
        cookinfoEntity.setCookName("lisi");
        cookinfoEntity.setCookPassword("123456");


//        CookDao cookDao = new CookDao();
        CookService cookService=new CookService();
        try {
            showJson(cookService.cookLogin(cookinfoEntity));

        } catch (MyException e) {
            e.printStackTrace();
            System.out.println(e.getErrorMsg());
        }
    }


    private static void toCode() {
        SHAencryption sha=new SHAencryption();
        try {
//            String uuid=UUID.randomUUID().toString();
            String uuid="566a7f44-fb31-440a-9d52-f83f9bdcaf48";

            String pass1=sha.encryptSHA("123456");
//            System.out.println(uuid);
            String pass2=sha.encryptSHA(pass1+uuid);
            String pass3=sha.encryptSHA(pass2);
            System.out.println(pass3);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showJson(Object obj){
        System.out.println(JSON.toJSONString(obj));
    }


}
