package com.zuxia.struts2.inter.service;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.bean.OrderInfo;
import com.zuxia.struts2.inter.dao.OrderDao;
import com.zuxia.struts2.inter.util.ServiceUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderService extends ServiceUtil {

    public void insertOrder(OrderInfo orderInfo) throws MyException {
        checkNull(orderInfo.getOrderDetailInfoList(),"订单菜品为空");
        checkNull(orderInfo.getOrderDetailInfoList().size(),"订单菜品为空");

        checkNull(orderInfo.getShopId(),"商铺id不能为空");

        //自动插入订单插入时间
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        orderInfo.setOrderTime(df.format(new Date()));
        orderInfo.setStatus(0);

        OrderDao orderDao = new OrderDao();
        orderDao.insertOrderAndDetails(orderInfo);

    }

    /*
     * 查询订单的详细信息
     * 连表查询全部订单信息
     * 1.先查基础信息，封装到orderinfo
     * 2.再查，详细菜品列表，封装到ArrlyList《sfdfdfd》  再把这个arrlylist封装到arderinfo
     * 合并后的订单信息（OrderinfoEntity-OrderdetailsinfoEntity）
     * */

}

