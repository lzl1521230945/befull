package com.zuxia.struts2.inter.service;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.inter.dao.ShopDao;
import com.zuxia.struts2.inter.entity.ShopinfoEntity;
import com.zuxia.struts2.inter.util.ServiceUtil;

import java.util.List;

public class ShopService extends ServiceUtil {
    /**
     * 添加商铺的方法
     * 需要传入商铺的名字
     * 无返回值
     * @param shopName
     * @throws MyException
     */
    public void addShop(String shopName) throws MyException {
        String s = checkNull(shopName, "添加商铺时，商铺名字不能为空");
        ShopDao shopDao = new ShopDao();
        shopDao.insertShopInfo(s);
    }

    /**
     * 查询商铺所有信息
     * 返回一个list集合
     * 2018年7月18日
     * @return
     * @throws MyException
     */
    public List<ShopinfoEntity> queryAllShopInfo() throws MyException {
        ShopDao shopDao = new ShopDao();
        List<ShopinfoEntity> shopinfoEntities = shopDao.selectAllShop();
        return  (List<ShopinfoEntity>) checkNull(shopinfoEntities,"查询商铺的结果为空");
    }

    /**
     * 通过商铺id查该商铺的信息
     *
     * @param shopid
     * @return
     * @throws MyException
     */
    public ShopinfoEntity selectShopInfoByid(int shopid) throws MyException {
        checkNull(shopid,"商铺id不能为空");
        ShopDao shopDao = new ShopDao();
        return (ShopinfoEntity)checkNull(shopDao.selectShopInfoByid(shopid),"你查询的店铺为空");
    }
}
