package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "foodtypeinfo", schema = "ordersystemdb", catalog = "")
public class FoodtypeinfoEntity {
    private int foodTypeId;
    private String foodTypeName;
    private Integer shopId;

    @Id
    @Column(name = "foodTypeId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getFoodTypeId() {
        return foodTypeId;
    }

    public void setFoodTypeId(int foodTypeId) {
        this.foodTypeId = foodTypeId;
    }

    @Basic
    @Column(name = "foodTypeName")
    public String getFoodTypeName() {
        return foodTypeName;
    }

    public void setFoodTypeName(String foodTypeName) {
        this.foodTypeName = foodTypeName;
    }

    @Basic
    @Column(name = "shopId")
    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoodtypeinfoEntity that = (FoodtypeinfoEntity) o;
        return foodTypeId == that.foodTypeId &&
                Objects.equals(foodTypeName, that.foodTypeName) &&
                Objects.equals(shopId, that.shopId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(foodTypeId, foodTypeName, shopId);
    }
}
