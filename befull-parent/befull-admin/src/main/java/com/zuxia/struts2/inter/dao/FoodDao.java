package com.zuxia.struts2.inter.dao;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.base.util.HibernateUtils;
import com.zuxia.struts2.inter.bean.FoodInfo;
import com.zuxia.struts2.inter.entity.EvaluateinfoEntity;
import com.zuxia.struts2.inter.entity.FooddetailsinfoEntity;
import com.zuxia.struts2.inter.entity.FoodinfoEntity;
import com.zuxia.struts2.inter.util.DaoUtil;
import com.zuxia.struts2.inter.util.ServiceUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class FoodDao {
    Session session=null;
    Transaction transaction=null;
    /**
     *根据食物id查询食物详细信息
     *需要传入【foodIds】
     */
    public FoodInfo selectFoodDetailbyId(int foodIds) throws MyException {
        List list = null;
        FoodInfo foodInfo;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from FoodinfoEntity a left join FooddetailsinfoEntity b on" +
                    " a.foodId=b.foodId where a.foodId = ? ");
            //填写站位符，根据id查询数据
            query.setParameter(0,foodIds);
            //使用query对象的list对象得到数据
            list = query.list();

            foodInfo=DaoUtil.getTogether(list.get(0));

        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        return foodInfo;
    }
    /**
     * 根据食物id查询食物评价
     * 【foodIds】
     */
    public ArrayList<EvaluateinfoEntity> selectEvaluateById(int foodIds) throws MyException {
        List list = null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from EvaluateinfoEntity where foodId = ? ");
            //填写站位符，根据id查询数据
            query.setParameter(0,foodIds);
            //使用query对象的list对象得到数据
            list = query.list();
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        return (ArrayList<EvaluateinfoEntity>)list;
    }
    /**
     * 根据食物Id添加食物评价
     * 【foodId userEvaluate】
     */
    public void insertEvaluateInfoodId (EvaluateinfoEntity EvaluateinfoEntity) throws MyException {
        try {
            //加载工厂
             session = HibernateUtils.getSession();
            //加载事务
             transaction = session.beginTransaction();
            //实例化食物类型表
            EvaluateinfoEntity evaluateinfoEntity = new EvaluateinfoEntity();
            //设置值
            evaluateinfoEntity.setFoodId(EvaluateinfoEntity.getFoodId());
            evaluateinfoEntity.setUserEvaluate(EvaluateinfoEntity.getUserEvaluate());
            //持久化添加
            session.save(evaluateinfoEntity);
            //提交事务
            transaction.commit();
        }catch (Exception e){
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
    }
    /*
    * 删除失误评价表
    * 通过传递食物Id进行删除
    * */
    public void deleteEvaluateInfoodById(int foodId) throws MyException {
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();
            //加载要修改的数据
            EvaluateinfoEntity evaluateinfoEntity = session.get(EvaluateinfoEntity.class, new Integer(foodId));
            //执行删除
            session.delete(evaluateinfoEntity);
            //提交事务
            transaction.commit();
        }catch (Exception e){
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }

    }

    /*
    * 添加食物信息表及详细信息
    * 通过接受一个集合
    * */
    public int  insertFoodDetailsInfo(FoodInfo foodInfo) throws MyException {
        int save;
        try {
            //加载工厂
             session = HibernateUtils.getSession();
             //加载事务
             transaction = session.beginTransaction();
             //创建一个菜品实体类
            FoodinfoEntity foodinfoEntity = new FoodinfoEntity();
            foodinfoEntity.setFoodName(foodInfo.getFoodName());
            foodinfoEntity.setFoodTypeId(foodInfo.getFoodTypeId());
            foodinfoEntity.setShopId(foodInfo.getShopId());
            //添加菜品信息
            save = (int)session.save(foodinfoEntity);
            //判断添加菜品是否成功
            if(save!=0){
                //创建一个菜品详细信息对象
                FooddetailsinfoEntity fooddetailsinfoEntity = new FooddetailsinfoEntity();
                //给该对象添值
                fooddetailsinfoEntity.setFoodId(save);
                fooddetailsinfoEntity.setFoodImage(foodInfo.getFoodImage());
                fooddetailsinfoEntity.setFoodprice(foodInfo.getFoodprice());
                fooddetailsinfoEntity.setFoodDetailsId(foodInfo.getFoodDetailsId());
                fooddetailsinfoEntity.setConsumptionQuantity(foodInfo.getConsumptionQuantity());
                fooddetailsinfoEntity.setFoodAttribute(foodInfo.getFoodAttribute());
                fooddetailsinfoEntity.setFoodWeight(foodInfo.getFoodWeight());
                //添加菜品详细信息
                session.save(fooddetailsinfoEntity);
            }
            transaction.commit();
            return save;
        }catch (Exception e){
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
    }

    /**
     *修改菜品信息
     * 需要传入一个修改过后的信息
     *注意：修改的id不能发生变化
     * 无返回值
     * 2018年7月19号
     * @param foodinfoEntity
     * @throws MyException
     */
    public void updateFoodById(FoodinfoEntity foodinfoEntity) throws MyException {

        DaoUtil.dataUpdate(foodinfoEntity);
    }

    /**
     * 修改菜品详情
     * 需要传入一个修改后的菜品详情对象
     * 注意：修改的id不能发生变化
     * 无返回值
     * 2018年7月19日
     * @param fooddetailsinfoEntity
     * @throws MyException
     */
    public void updateDetailByid(FooddetailsinfoEntity fooddetailsinfoEntity) throws MyException {

           DaoUtil.dataUpdate(fooddetailsinfoEntity);

    }

    public  void updateFoodAndDetailByid(FoodInfo foodInfo) throws MyException {
        int save;
        try {
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();

            //执行修改
            FoodinfoEntity foodinfoEntity = new FoodinfoEntity(
                    foodInfo.getFoodId(),
                    foodInfo.getFoodName(),
                    foodInfo.getFoodTypeId(),
                    foodInfo.getShopId()
            );
            session.update(foodinfoEntity);

            FooddetailsinfoEntity fooddetailsinfoEntity = new FooddetailsinfoEntity(
                    "",
                    foodInfo.getFoodprice(),
                    foodInfo.getConsumptionQuantity(),
                    foodInfo.getFoodId(),
                    foodInfo.getFoodAttribute(),
                    foodInfo.getFoodWeight()
            );
            session.update(fooddetailsinfoEntity);

            //提交事务
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        } finally {
            session.close();
        }
    }


    /*
    * 根据食物Id删除该类菜
    * */
    public void deleteFoodDetailsById(int foodId) throws MyException {
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();
            //加载要修改的数据
            FoodinfoEntity foodinfoEntity1 = session.get(FoodinfoEntity.class, new Integer(foodId));
            //执行删除
            session.delete(foodinfoEntity1);
            //加载要修改的数据
            FooddetailsinfoEntity fooddetailsinfoEntity = session.get(FooddetailsinfoEntity.class, new Integer(foodId));
            //执行删除
            session.delete(fooddetailsinfoEntity);
            //提交事务
            transaction.commit();
        }catch (Exception e){
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
    }


}

