package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "orderinfo", schema = "ordersystemdb", catalog = "")
public class OrderinfoEntity {
    private int orderId;
    private String orderRemarks;
    private Integer seatId;
    private Integer shopId;
    private String orderTime;
    private Integer status;

    @Id
    @Column(name = "orderId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "orderRemarks")
    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    @Basic
    @Column(name = "seatId")
    public Integer getSeatId() {
        return seatId;
    }

    public void setSeatId(Integer seatId) {
        this.seatId = seatId;
    }

    @Basic
    @Column(name = "shopId")
    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderinfoEntity that = (OrderinfoEntity) o;
        return orderId == that.orderId &&
                Objects.equals(orderRemarks, that.orderRemarks) &&
                Objects.equals(seatId, that.seatId) &&
                Objects.equals(shopId, that.shopId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(orderId, orderRemarks, seatId, shopId);
    }

    @Basic
    @Column(name = "orderTime")
    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    @Basic
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OrderinfoEntity(String orderRemarks, Integer seatId,
                           Integer shopId, String orderTime,
                           Integer status) {
        this.orderRemarks = orderRemarks;
        this.seatId = seatId;
        this.shopId = shopId;
        this.orderTime = orderTime;
        this.status = status;
    }
}
