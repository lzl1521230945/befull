package com.zuxia.struts2.inter.bean;

import com.zuxia.struts2.inter.entity.OrderdetailsinfoEntity;
import com.zuxia.struts2.inter.entity.OrderinfoEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class OrderInfo  {
    private int orderId;
    private String orderRemarks; //订单备注信息
    private Integer seatId;       //座位号id
    private Integer shopId;       //商铺id
    private String orderTime;     //订单时间
    private Integer status;

    private ArrayList<OrderDetailInfo> OrderDetailInfoList ;

    public OrderInfo(){

    }

    public OrderInfo(String orderRemarks, Integer seatId, Integer shopId, String orderTime) {
        this.orderRemarks = orderRemarks;
        this.seatId = seatId;
        this.shopId = shopId;
        this.orderTime = orderTime;
    }


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    public Integer getSeatId() {
        return seatId;
    }

    public void setSeatId(Integer seatId) {
        this.seatId = seatId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<OrderDetailInfo> getOrderDetailInfoList() {
        return OrderDetailInfoList;
    }

    public void setOrderDetailInfoList(ArrayList<OrderDetailInfo> orderDetailInfoList) {
        OrderDetailInfoList = orderDetailInfoList;
    }
}
