package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "orderdetailsinfo", schema = "ordersystemdb", catalog = "")
public class OrderdetailsinfoEntity {
    private int orderDetailsId;
    private String foodName;
    private int foodNumber;
    private String foodAttribute;
    private String foodWeight;
    private int price;
    private int totalPrice;
    private Integer orderId;

    @Id
    @Column(name = "orderDetailsId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(int orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    @Basic
    @Column(name = "foodName")
    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    @Basic
    @Column(name = "foodNumber")
    public int getFoodNumber() {
        return foodNumber;
    }

    public void setFoodNumber(int foodNumber) {
        this.foodNumber = foodNumber;
    }

    @Basic
    @Column(name = "foodAttribute")
    public String getFoodAttribute() {
        return foodAttribute;
    }

    public void setFoodAttribute(String foodAttribute) {
        this.foodAttribute = foodAttribute;
    }

    @Basic
    @Column(name = "foodWeight")
    public String getFoodWeight() {
        return foodWeight;
    }

    public void setFoodWeight(String foodWeight) {
        this.foodWeight = foodWeight;
    }

    @Basic
    @Column(name = "price")
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Basic
    @Column(name = "totalPrice")
    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "orderId")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderdetailsinfoEntity that = (OrderdetailsinfoEntity) o;
        return orderDetailsId == that.orderDetailsId &&
                foodNumber == that.foodNumber &&
                price == that.price &&
                totalPrice == that.totalPrice &&
                Objects.equals(foodName, that.foodName) &&
                Objects.equals(foodAttribute, that.foodAttribute) &&
                Objects.equals(foodWeight, that.foodWeight) &&
                Objects.equals(orderId, that.orderId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(orderDetailsId, foodName, foodNumber, foodAttribute, foodWeight, price, totalPrice, orderId);
    }

    public OrderdetailsinfoEntity( String foodName, int foodNumber, String foodAttribute, String foodWeight, int price, int totalPrice, Integer orderId) {
        this.foodName = foodName;
        this.foodNumber = foodNumber;
        this.foodAttribute = foodAttribute;
        this.foodWeight = foodWeight;
        this.price = price;
        this.totalPrice = totalPrice;
        this.orderId = orderId;
    }
}
