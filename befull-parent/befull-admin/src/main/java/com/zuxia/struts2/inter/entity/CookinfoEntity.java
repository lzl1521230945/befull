package com.zuxia.struts2.inter.entity;

import javax.persistence.*;

@Entity
@Table(name = "cookinfo", schema = "ordersystemdb", catalog = "")
public class CookinfoEntity {
    private int cookId;
    private String cookName;
    private String cookPassword;
    private String salt;
    private Integer shopId;

    @Id
    @Column(name = "cookId")
    public int getCookId() {
        return cookId;
    }

    public void setCookId(int cookId) {
        this.cookId = cookId;
    }

    @Basic
    @Column(name = "cookName")
    public String getCookName() {
        return cookName;
    }

    public void setCookName(String cookName) {
        this.cookName = cookName;
    }

    @Basic
    @Column(name = "cookPassword")
    public String getCookPassword() {
        return cookPassword;
    }

    public void setCookPassword(String cookPassword) {
        this.cookPassword = cookPassword;
    }

    @Basic
    @Column(name = "salt")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Basic
    @Column(name = "shopId")
    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CookinfoEntity that = (CookinfoEntity) o;

        if (cookId != that.cookId) return false;
        if (cookName != null ? !cookName.equals(that.cookName) : that.cookName != null) return false;
        if (cookPassword != null ? !cookPassword.equals(that.cookPassword) : that.cookPassword != null) return false;
        if (salt != null ? !salt.equals(that.salt) : that.salt != null) return false;
        if (shopId != null ? !shopId.equals(that.shopId) : that.shopId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cookId;
        result = 31 * result + (cookName != null ? cookName.hashCode() : 0);
        result = 31 * result + (cookPassword != null ? cookPassword.hashCode() : 0);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (shopId != null ? shopId.hashCode() : 0);
        return result;
    }
}
