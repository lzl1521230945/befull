package com.zuxia.struts2.inter.bean;

import java.util.ArrayList;

public class FoodTypeInfo {
    private int foodTypeId;
    private String foodTypeName;
    private Integer shopId;
    private ArrayList<Integer> foodIdsOfType;

    public FoodTypeInfo() {
    }

    public FoodTypeInfo(int foodTypeId, String foodTypeName, Integer shopId, ArrayList<Integer> foodIdsOfType) {
        this.foodTypeId = foodTypeId;
        this.foodTypeName = foodTypeName;
        this.shopId = shopId;
        this.foodIdsOfType = foodIdsOfType;
    }

    public int getFoodTypeId() {
        return foodTypeId;
    }

    public void setFoodTypeId(int foodTypeId) {
        this.foodTypeId = foodTypeId;
    }

    public String getFoodTypeName() {
        return foodTypeName;
    }

    public void setFoodTypeName(String foodTypeName) {
        this.foodTypeName = foodTypeName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public ArrayList<Integer> getFoodIdsOfType() {
        return foodIdsOfType;
    }

    public void setFoodIdsOfType(ArrayList<Integer> foodIdsOfType) {
        this.foodIdsOfType = foodIdsOfType;
    }
}
