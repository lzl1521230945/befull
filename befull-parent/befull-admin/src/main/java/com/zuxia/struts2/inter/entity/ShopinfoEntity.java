package com.zuxia.struts2.inter.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "shopinfo", schema = "ordersystemdb", catalog = "")
public class ShopinfoEntity {
    private int shopId;
    private String shopName;
    private String shopIntroduction;

    @Id
    @Column(name = "shopId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    @Basic
    @Column(name = "shopName")
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShopinfoEntity that = (ShopinfoEntity) o;
        return shopId == that.shopId &&
                Objects.equals(shopName, that.shopName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(shopId, shopName);
    }

    @Basic
    @Column(name = "shopIntroduction")
    public String getShopIntroduction() {
        return shopIntroduction;
    }

    public void setShopIntroduction(String shopIntroduction) {
        this.shopIntroduction = shopIntroduction;
    }
}
