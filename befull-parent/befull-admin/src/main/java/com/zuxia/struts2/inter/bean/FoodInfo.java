package com.zuxia.struts2.inter.bean;

import java.util.ArrayList;

public class FoodInfo {
    private int foodId;
    private String foodName;
    private Integer foodTypeId; //食物类型id
    private Integer shopId; //商铺id
    private int foodDetailsId;//食物详细id
    private String foodImage; //图片
    private int foodprice;  // 价格
    private Integer consumptionQuantity; //月销售
    private String foodAttribute; //食物属性
    private String foodWeight;  //食物分量
    private String[] foodAttributeList;//食品属性集合
    private String[] foodWeightList;  //食物分量集合
    private FileInfo imgFile;



    public FoodInfo() {
    }

    public FoodInfo(int foodId, String foodName, Integer foodTypeId, Integer shopId,
                    String foodImage, int foodprice, Integer consumptionQuantity,
                    String foodAttribute, String foodWeight, String[] foodAttributeList, String[] foodWeightList) {
        this.foodId = foodId;
        this.foodName = foodName;
        this.foodTypeId = foodTypeId;
        this.shopId = shopId;
        this.foodImage = foodImage;
        this.foodprice = foodprice;
        this.consumptionQuantity = consumptionQuantity;
        this.foodAttribute = foodAttribute;
        this.foodWeight = foodWeight;
        this.foodAttributeList = foodAttributeList;
        this.foodWeightList = foodWeightList;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public Integer getFoodTypeId() {
        return foodTypeId;
    }

    public void setFoodTypeId(Integer foodTypeId) {
        this.foodTypeId = foodTypeId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public int getFoodDetailsId() {
        return foodDetailsId;
    }

    public void setFoodDetailsId(int foodDetailsId) {
        this.foodDetailsId = foodDetailsId;
    }

    public String getFoodImage() {
        return foodImage;
    }

    public void setFoodImage(String foodImage) {
        this.foodImage = foodImage;
    }

    public int getFoodprice() {
        return foodprice;
    }

    public void setFoodprice(int foodprice) {
        this.foodprice = foodprice;
    }

    public Integer getConsumptionQuantity() {
        return consumptionQuantity;
    }

    public void setConsumptionQuantity(Integer consumptionQuantity) {
        this.consumptionQuantity = consumptionQuantity;
    }

    public String getFoodAttribute() {
        return foodAttribute;
    }

    public void setFoodAttribute(String foodAttribute) {
        this.foodAttribute = foodAttribute;
    }

    public String getFoodWeight() {
        return foodWeight;
    }

    public void setFoodWeight(String foodWeight) {
        this.foodWeight = foodWeight;
    }

    public String[] getFoodAttributeList() {
        return foodAttributeList;
    }

    public void setFoodAttributeList(String[] foodAttributeList) {
        this.foodAttributeList = foodAttributeList;
    }

    public String[] getFoodWeightList() {
        return foodWeightList;
    }

    public void setFoodWeightList(String[] foodWeightList) {
        this.foodWeightList = foodWeightList;
    }

    public FileInfo getImgFile() {
        return imgFile;
    }

    public void setImgFile(FileInfo imgFile) {
        this.imgFile = imgFile;
    }
}
