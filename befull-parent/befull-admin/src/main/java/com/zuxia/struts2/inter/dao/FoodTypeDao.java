package com.zuxia.struts2.inter.dao;

import com.zuxia.struts2.base.exception.MyException;
import com.zuxia.struts2.base.util.HibernateUtils;
import com.zuxia.struts2.inter.entity.FooddetailsinfoEntity;
import com.zuxia.struts2.inter.entity.FoodinfoEntity;
import com.zuxia.struts2.inter.entity.FoodtypeinfoEntity;
import com.zuxia.struts2.inter.util.DaoUtil;
import com.zuxia.struts2.inter.util.ServiceUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class FoodTypeDao {
    Session session =null;
    Transaction transaction=null;

    /*
     * 根据商铺id查询食物信息类型表
     * 返回list集合
     * */
    public ArrayList<FoodtypeinfoEntity> selectFoodTypeListByShopId(int shopId) throws MyException {
        List list = null;
        try {
            session = HibernateUtils.getSession();
            //得到query对象并写入hql语句
            Query from_foodtypeinfoEntity = session.createQuery("from FoodtypeinfoEntity where shopId = ?");
            //填写站位符，根据id查询数据
            from_foodtypeinfoEntity.setParameter(0,shopId);
            //使用query对象的list方法得到数据集合
            list = from_foodtypeinfoEntity.list();

        }catch (Exception e){
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        return (ArrayList<FoodtypeinfoEntity>)list;
    }
    /*
     * 根据食物类型id查询食物信息
     * */
    public ArrayList<FoodinfoEntity> selectFoodlistByTypeId(int foodTypeId ) throws MyException {
        List list = null;
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            // 1、得到query对象，并写入hql语句
            Query query = session.createQuery("from FoodinfoEntity where foodTypeId = ?  ");
            //填写站位符，根据id查询数据
            query.setParameter(0,foodTypeId);
            //使用query对象的list方法得到数据集合
            list = query.list();


        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
        return  (ArrayList<FoodinfoEntity>)list;

    }

    /*
     *添加食物类型
     * 【shopId  foodTypeName】
     * */
    public void insertFoodType(String fooTypename) throws MyException {
        try {
            //加载工厂
            session = HibernateUtils.getSession();
            //加载事务
            transaction = session.beginTransaction();
            //实例化食物类型表
            FoodtypeinfoEntity foodtypeinfoEntity = new FoodtypeinfoEntity();
            foodtypeinfoEntity.setFoodTypeName(fooTypename);
            //持久化添加
            session.save(foodtypeinfoEntity);
            //提交事务
            transaction.commit();

        }catch (Exception e){
            e.printStackTrace();
            //回滚事务
            transaction.rollback();
            throw new MyException("数据库异常，请联系管理员！");
        }finally {
            session.close();
        }
    }
    /*
    * 修改商品类型信息
    * 根据商铺类型传递的对象进行修改
    * */
    public void updateFoodTypeInfo(FoodtypeinfoEntity foodtypeinfoEntity) throws MyException {

        DaoUtil.dataUpdate(foodtypeinfoEntity);

    }
    /*
    * 删除商品类型
    * 根据商品类型传递的id进行删除商品类型！！！！！！！！！！！！！！！！！！！！！需要检测该菜品类型下是否有关联的菜品
    * */
    public void deleteFoodTypeById(int foodTypeId) throws MyException {

        if(this.selectFoodlistByTypeId(foodTypeId)==null){
            throw new MyException("该食物类型下有数据，无法进行操作！");
        }else{
            try {
                //加载工厂
                session = HibernateUtils.getSession();
                //加载事务
                transaction = session.beginTransaction();
                //加载要修改的数据
                FoodtypeinfoEntity foodtypeinfoEntity = session.get(FoodtypeinfoEntity.class, new Integer(foodTypeId));
                //执行修改
                session.update(foodtypeinfoEntity);
                //提交事务
                transaction.commit();
            }catch (Exception e){
                e.printStackTrace();
                //回滚事务
                transaction.rollback();
                throw new MyException("数据库异常，请联系管理员！");
            }finally {
                session.close();
            }
        }

    }


}