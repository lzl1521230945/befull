/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : ordersystemdb

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-07-23 23:09:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admininfo`
-- ----------------------------
DROP TABLE IF EXISTS `admininfo`;
CREATE TABLE `admininfo` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(20) NOT NULL,
  `adminPassword` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`adminId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admininfo
-- ----------------------------
INSERT INTO `admininfo` VALUES ('1', 'admin', '6d38f4a7af5606b3d511d5f137f35b5dff905435', '2b3953e2-08bf-43de-80b1-89f7d2f3172f');

-- ----------------------------
-- Table structure for `cookinfo`
-- ----------------------------
DROP TABLE IF EXISTS `cookinfo`;
CREATE TABLE `cookinfo` (
  `cookId` int(11) NOT NULL AUTO_INCREMENT,
  `cookName` varchar(20) NOT NULL,
  `cookPassword` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `shopId` int(11) DEFAULT NULL,
  PRIMARY KEY (`cookId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cookinfo
-- ----------------------------
INSERT INTO `cookinfo` VALUES ('1', 'zhangsang', 'c760cf21e0e03b86c7edeefcc1b22634a0e0c8c0', '1a9aa698-1d92-4948-a276-4e941ae026c6', '1');
INSERT INTO `cookinfo` VALUES ('2', 'lisi', '91d1e807b375ea9ed5479714dc816dba24b9b672', '566a7f44-fb31-440a-9d52-f83f9bdcaf48', '2');
INSERT INTO `cookinfo` VALUES ('3', 'wangwu', '4c7c25374b08a516924623dc7283e0cc1ed9a183', '6cf5f189-121f-4f9b-9b45-d5f1f347eeb0', '3');
INSERT INTO `cookinfo` VALUES ('4', 'java46', 'b35106266fc24a422c257d4481e27553597fb4cd', '5071f409-6e27-4437-b677-9836364d09b5', '2');

-- ----------------------------
-- Table structure for `evaluateinfo`
-- ----------------------------
DROP TABLE IF EXISTS `evaluateinfo`;
CREATE TABLE `evaluateinfo` (
  `evaluateId` int(11) NOT NULL AUTO_INCREMENT,
  `userEvaluate` varchar(225) DEFAULT NULL,
  `foodId` int(11) DEFAULT NULL,
  PRIMARY KEY (`evaluateId`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of evaluateinfo
-- ----------------------------
INSERT INTO `evaluateinfo` VALUES ('1', '太好吃了', '1');
INSERT INTO `evaluateinfo` VALUES ('2', '非常好吃', '1');
INSERT INTO `evaluateinfo` VALUES ('3', '难以忘怀', '1');
INSERT INTO `evaluateinfo` VALUES ('4', '给一百个赞', '1');
INSERT INTO `evaluateinfo` VALUES ('5', '非常好吃', '2');
INSERT INTO `evaluateinfo` VALUES ('6', '难以忘怀', '2');
INSERT INTO `evaluateinfo` VALUES ('7', '给一百个赞', '2');
INSERT INTO `evaluateinfo` VALUES ('8', '太好吃了', '2');
INSERT INTO `evaluateinfo` VALUES ('9', '非常好吃', '3');
INSERT INTO `evaluateinfo` VALUES ('10', '难以忘怀', '3');
INSERT INTO `evaluateinfo` VALUES ('11', '给一百个赞', '3');
INSERT INTO `evaluateinfo` VALUES ('12', '太好吃了', '3');
INSERT INTO `evaluateinfo` VALUES ('13', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('14', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('15', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('16', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('17', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('18', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('19', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('20', '太好吃了', '4');
INSERT INTO `evaluateinfo` VALUES ('21', '太好吃了', '5');
INSERT INTO `evaluateinfo` VALUES ('22', '太好吃了', '5');
INSERT INTO `evaluateinfo` VALUES ('23', '太好吃了', '5');
INSERT INTO `evaluateinfo` VALUES ('24', '太好吃了', '5');
INSERT INTO `evaluateinfo` VALUES ('25', '太好吃了', '6');
INSERT INTO `evaluateinfo` VALUES ('26', '太好吃了', '6');
INSERT INTO `evaluateinfo` VALUES ('27', '太好吃了', '6');
INSERT INTO `evaluateinfo` VALUES ('28', '太好吃了', '6');
INSERT INTO `evaluateinfo` VALUES ('29', '太好吃了', '7');
INSERT INTO `evaluateinfo` VALUES ('30', '太好吃了', '7');
INSERT INTO `evaluateinfo` VALUES ('31', '太好吃了', '7');
INSERT INTO `evaluateinfo` VALUES ('32', '太好吃了', '7');
INSERT INTO `evaluateinfo` VALUES ('33', '太好吃了', '8');
INSERT INTO `evaluateinfo` VALUES ('34', '太好吃了', '8');
INSERT INTO `evaluateinfo` VALUES ('35', '太好吃了', '8');
INSERT INTO `evaluateinfo` VALUES ('36', '太好吃了', '8');
INSERT INTO `evaluateinfo` VALUES ('37', '太好吃了', '9');
INSERT INTO `evaluateinfo` VALUES ('38', '太好吃了', '9');
INSERT INTO `evaluateinfo` VALUES ('39', '太好吃了', '9');
INSERT INTO `evaluateinfo` VALUES ('40', '太好吃了', '9');
INSERT INTO `evaluateinfo` VALUES ('41', '太好吃了', '10');
INSERT INTO `evaluateinfo` VALUES ('42', '太好吃了', '10');
INSERT INTO `evaluateinfo` VALUES ('43', '太好吃了', '10');
INSERT INTO `evaluateinfo` VALUES ('44', '太好吃了', '10');
INSERT INTO `evaluateinfo` VALUES ('45', '太好吃了', '11');
INSERT INTO `evaluateinfo` VALUES ('46', '太好吃了', '11');
INSERT INTO `evaluateinfo` VALUES ('47', '太好吃了', '11');
INSERT INTO `evaluateinfo` VALUES ('48', '太好吃了', '11');
INSERT INTO `evaluateinfo` VALUES ('49', '太好吃了', '12');
INSERT INTO `evaluateinfo` VALUES ('50', '太好吃了', '12');
INSERT INTO `evaluateinfo` VALUES ('51', '太好吃了', '12');
INSERT INTO `evaluateinfo` VALUES ('52', '太好吃了', '12');
INSERT INTO `evaluateinfo` VALUES ('53', '太好吃了', '13');
INSERT INTO `evaluateinfo` VALUES ('54', '太好吃了', '13');
INSERT INTO `evaluateinfo` VALUES ('56', '太好吃了', '13');
INSERT INTO `evaluateinfo` VALUES ('57', '太好吃了', '14');
INSERT INTO `evaluateinfo` VALUES ('58', '太好吃了', '14');
INSERT INTO `evaluateinfo` VALUES ('59', '太好吃了', '14');
INSERT INTO `evaluateinfo` VALUES ('60', '太好吃了', '14');
INSERT INTO `evaluateinfo` VALUES ('61', '太好吃了', '15');
INSERT INTO `evaluateinfo` VALUES ('62', '太好吃了', '15');
INSERT INTO `evaluateinfo` VALUES ('63', '太好吃了', '15');
INSERT INTO `evaluateinfo` VALUES ('64', '太好吃了', '15');
INSERT INTO `evaluateinfo` VALUES ('65', '太好吃了', '16');
INSERT INTO `evaluateinfo` VALUES ('66', '太好吃了', '16');
INSERT INTO `evaluateinfo` VALUES ('67', '太好吃了', '16');
INSERT INTO `evaluateinfo` VALUES ('68', '太好吃了', '16');
INSERT INTO `evaluateinfo` VALUES ('69', '太好吃了', '17');
INSERT INTO `evaluateinfo` VALUES ('70', '太好吃了', '17');
INSERT INTO `evaluateinfo` VALUES ('71', '太好吃了', '17');
INSERT INTO `evaluateinfo` VALUES ('72', '太好吃了', '17');
INSERT INTO `evaluateinfo` VALUES ('73', '太好吃了', '18');
INSERT INTO `evaluateinfo` VALUES ('74', '太好吃了', '18');
INSERT INTO `evaluateinfo` VALUES ('75', '太好吃了', '18');
INSERT INTO `evaluateinfo` VALUES ('76', '太好吃了', '18');
INSERT INTO `evaluateinfo` VALUES ('77', '太好吃了', '19');
INSERT INTO `evaluateinfo` VALUES ('78', '太好吃了', '19');
INSERT INTO `evaluateinfo` VALUES ('79', '太好吃了', '19');
INSERT INTO `evaluateinfo` VALUES ('80', '太好吃了', '19');
INSERT INTO `evaluateinfo` VALUES ('81', '太好吃了', '20');
INSERT INTO `evaluateinfo` VALUES ('82', '太好吃了', '20');
INSERT INTO `evaluateinfo` VALUES ('83', '太好吃了', '20');
INSERT INTO `evaluateinfo` VALUES ('85', '太好吃了', '13');

-- ----------------------------
-- Table structure for `fooddetailsinfo`
-- ----------------------------
DROP TABLE IF EXISTS `fooddetailsinfo`;
CREATE TABLE `fooddetailsinfo` (
  `foodDetailsId` int(11) NOT NULL AUTO_INCREMENT,
  `foodImage` varchar(225) DEFAULT NULL,
  `foodprice` decimal(10,0) NOT NULL,
  `consumptionQuantity` int(11) DEFAULT NULL,
  `foodAttribute` varchar(20) DEFAULT NULL,
  `foodWeight` varchar(20) DEFAULT NULL,
  `foodId` int(11) DEFAULT NULL,
  PRIMARY KEY (`foodDetailsId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fooddetailsinfo
-- ----------------------------
INSERT INTO `fooddetailsinfo` VALUES ('1', '麻婆豆腐.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '1');
INSERT INTO `fooddetailsinfo` VALUES ('2', '鱼香肉丝.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '2');
INSERT INTO `fooddetailsinfo` VALUES ('3', '竹笋炒肉.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '3');
INSERT INTO `fooddetailsinfo` VALUES ('4', '甜甜圈.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '4');
INSERT INTO `fooddetailsinfo` VALUES ('5', '蚂蚁上树.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '5');
INSERT INTO `fooddetailsinfo` VALUES ('6', '鸡皮鲟龙.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '6');
INSERT INTO `fooddetailsinfo` VALUES ('7', '蟹黄鲜菇.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '7');
INSERT INTO `fooddetailsinfo` VALUES ('8', '青瓜拼腰.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '8');
INSERT INTO `fooddetailsinfo` VALUES ('9', '鸡肉拉皮.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '9');
INSERT INTO `fooddetailsinfo` VALUES ('10', '菠萝拼火鹅.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '10');
INSERT INTO `fooddetailsinfo` VALUES ('11', '夜合虾仁.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '11');
INSERT INTO `fooddetailsinfo` VALUES ('12', '青瓜拼腰花.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '12');
INSERT INTO `fooddetailsinfo` VALUES ('13', '麻婆豆腐.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '13');
INSERT INTO `fooddetailsinfo` VALUES ('14', '鱼香肉丝.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '14');
INSERT INTO `fooddetailsinfo` VALUES ('15', '竹笋炒肉.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '15');
INSERT INTO `fooddetailsinfo` VALUES ('16', '香辣土豆.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '16');
INSERT INTO `fooddetailsinfo` VALUES ('17', '蚂蚁上树.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '17');
INSERT INTO `fooddetailsinfo` VALUES ('18', '鸡皮鲟龙.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '18');
INSERT INTO `fooddetailsinfo` VALUES ('19', '蟹黄鲜菇.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '19');
INSERT INTO `fooddetailsinfo` VALUES ('20', '青瓜拼腰.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '20');
INSERT INTO `fooddetailsinfo` VALUES ('21', '鸡肉拉皮.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '21');
INSERT INTO `fooddetailsinfo` VALUES ('22', '菠萝拼火鹅.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '22');
INSERT INTO `fooddetailsinfo` VALUES ('23', '夜合虾仁.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '23');
INSERT INTO `fooddetailsinfo` VALUES ('24', '青瓜拼腰花.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '24');
INSERT INTO `fooddetailsinfo` VALUES ('25', '麻婆豆腐.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '26');
INSERT INTO `fooddetailsinfo` VALUES ('26', '鱼香肉丝.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '27');
INSERT INTO `fooddetailsinfo` VALUES ('27', '竹笋炒肉.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '28');
INSERT INTO `fooddetailsinfo` VALUES ('28', '香辣土豆.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '29');
INSERT INTO `fooddetailsinfo` VALUES ('29', '蚂蚁上树.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '30');
INSERT INTO `fooddetailsinfo` VALUES ('30', '鸡皮鲟龙.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '31');
INSERT INTO `fooddetailsinfo` VALUES ('31', '蟹黄鲜菇.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '32');
INSERT INTO `fooddetailsinfo` VALUES ('32', '青瓜拼腰.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '33');
INSERT INTO `fooddetailsinfo` VALUES ('33', '鸡肉拉皮.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '34');
INSERT INTO `fooddetailsinfo` VALUES ('34', '菠萝拼火鹅.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '35');
INSERT INTO `fooddetailsinfo` VALUES ('35', '夜合虾仁.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '36');
INSERT INTO `fooddetailsinfo` VALUES ('36', '青瓜拼腰花.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '37');
INSERT INTO `fooddetailsinfo` VALUES ('37', '麻婆豆腐.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '38');
INSERT INTO `fooddetailsinfo` VALUES ('38', '鱼香肉丝.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '39');
INSERT INTO `fooddetailsinfo` VALUES ('39', '竹笋炒肉.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '40');
INSERT INTO `fooddetailsinfo` VALUES ('40', '香辣土豆.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '41');
INSERT INTO `fooddetailsinfo` VALUES ('41', '蚂蚁上树.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '42');
INSERT INTO `fooddetailsinfo` VALUES ('42', '鸡皮鲟龙.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '43');
INSERT INTO `fooddetailsinfo` VALUES ('43', '蟹黄鲜菇.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '44');
INSERT INTO `fooddetailsinfo` VALUES ('44', '青瓜拼腰.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '45');
INSERT INTO `fooddetailsinfo` VALUES ('45', '鸡肉拉皮.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '46');
INSERT INTO `fooddetailsinfo` VALUES ('46', '菠萝拼火鹅.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '47');
INSERT INTO `fooddetailsinfo` VALUES ('47', '夜合虾仁.jpg', '12', '150', '很辣,辣,一般,不辣', '大份,中份,小份', '48');

-- ----------------------------
-- Table structure for `foodinfo`
-- ----------------------------
DROP TABLE IF EXISTS `foodinfo`;
CREATE TABLE `foodinfo` (
  `foodId` int(11) NOT NULL AUTO_INCREMENT,
  `foodName` varchar(20) NOT NULL,
  `foodTypeId` int(11) DEFAULT NULL,
  `shopId` int(11) DEFAULT NULL,
  PRIMARY KEY (`foodId`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of foodinfo
-- ----------------------------
INSERT INTO `foodinfo` VALUES ('1', '麻婆豆腐', '13', '3');
INSERT INTO `foodinfo` VALUES ('2', '鱼香肉丝', '8', '2');
INSERT INTO `foodinfo` VALUES ('3', '竹笋炒肉', '15', '3');
INSERT INTO `foodinfo` VALUES ('4', '甜甜圈', '13', '3');
INSERT INTO `foodinfo` VALUES ('5', '蚂蚁上树', '4', '4');
INSERT INTO `foodinfo` VALUES ('6', '鸡皮鲟龙', '1', '4');
INSERT INTO `foodinfo` VALUES ('7', '蟹黄鲜菇', '11', '2');
INSERT INTO `foodinfo` VALUES ('8', '青瓜拼腰', '3', '4');
INSERT INTO `foodinfo` VALUES ('9', '鸡肉拉皮', '11', '2');
INSERT INTO `foodinfo` VALUES ('10', '菠萝拼火鹅', '12', '2');
INSERT INTO `foodinfo` VALUES ('11', '夜合虾仁', '15', '3');
INSERT INTO `foodinfo` VALUES ('12', '青瓜拼腰花', '1', '4');
INSERT INTO `foodinfo` VALUES ('13', '麻婆豆腐', '14', '3');
INSERT INTO `foodinfo` VALUES ('14', '鱼香肉丝', '11', '2');
INSERT INTO `foodinfo` VALUES ('15', '竹笋炒肉', '16', '3');
INSERT INTO `foodinfo` VALUES ('16', '香辣土豆', '16', '3');
INSERT INTO `foodinfo` VALUES ('17', '蚂蚁上树', '1', '4');
INSERT INTO `foodinfo` VALUES ('18', '鸡皮鲟龙', '2', '4');
INSERT INTO `foodinfo` VALUES ('19', '蟹黄鲜菇', '9', '2');
INSERT INTO `foodinfo` VALUES ('20', '青瓜拼腰', '3', '4');
INSERT INTO `foodinfo` VALUES ('21', '鸡肉拉皮', '10', '2');
INSERT INTO `foodinfo` VALUES ('22', '菠萝拼火鹅', '11', '2');
INSERT INTO `foodinfo` VALUES ('23', '夜合虾仁', '13', '3');
INSERT INTO `foodinfo` VALUES ('24', '青瓜拼腰花', '14', '3');
INSERT INTO `foodinfo` VALUES ('26', '麻婆豆腐', '15', '3');
INSERT INTO `foodinfo` VALUES ('27', '鱼香肉丝', '12', '2');
INSERT INTO `foodinfo` VALUES ('28', '竹笋炒肉', '14', '3');
INSERT INTO `foodinfo` VALUES ('29', '香辣土豆', '13', '3');
INSERT INTO `foodinfo` VALUES ('30', '蚂蚁上树', '1', '4');
INSERT INTO `foodinfo` VALUES ('31', '鸡皮鲟龙', '2', '4');
INSERT INTO `foodinfo` VALUES ('32', '蟹黄鲜菇', '9', '2');
INSERT INTO `foodinfo` VALUES ('33', '青瓜拼腰', '4', '4');
INSERT INTO `foodinfo` VALUES ('34', '鸡肉拉皮', '10', '2');
INSERT INTO `foodinfo` VALUES ('35', '菠萝拼火鹅', '11', '2');
INSERT INTO `foodinfo` VALUES ('36', '夜合虾仁', '14', '3');
INSERT INTO `foodinfo` VALUES ('37', '青瓜拼腰花', '15', '3');
INSERT INTO `foodinfo` VALUES ('38', '麻婆豆腐', '16', '3');
INSERT INTO `foodinfo` VALUES ('39', '鱼香肉丝', '9', '2');
INSERT INTO `foodinfo` VALUES ('40', '竹笋炒肉', '13', '3');
INSERT INTO `foodinfo` VALUES ('41', '香辣土豆', '14', '3');
INSERT INTO `foodinfo` VALUES ('42', '蚂蚁上树', '1', '4');
INSERT INTO `foodinfo` VALUES ('43', '鸡皮鲟龙', '2', '4');
INSERT INTO `foodinfo` VALUES ('44', '蟹黄鲜菇', '9', '2');
INSERT INTO `foodinfo` VALUES ('45', '青瓜拼腰', '3', '4');
INSERT INTO `foodinfo` VALUES ('46', '鸡肉拉皮', '10', '2');
INSERT INTO `foodinfo` VALUES ('47', '菠萝拼火鹅', '12', '2');
INSERT INTO `foodinfo` VALUES ('48', '夜合虾仁', '13', '3');
INSERT INTO `foodinfo` VALUES ('49', '青瓜拼腰花', '15', '3');

-- ----------------------------
-- Table structure for `foodtypeinfo`
-- ----------------------------
DROP TABLE IF EXISTS `foodtypeinfo`;
CREATE TABLE `foodtypeinfo` (
  `foodTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `foodTypeName` varchar(10) NOT NULL,
  `shopId` int(11) DEFAULT NULL,
  PRIMARY KEY (`foodTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of foodtypeinfo
-- ----------------------------
INSERT INTO `foodtypeinfo` VALUES ('1', '甜食', '4');
INSERT INTO `foodtypeinfo` VALUES ('2', '冷食', '4');
INSERT INTO `foodtypeinfo` VALUES ('3', '凉菜', '4');
INSERT INTO `foodtypeinfo` VALUES ('4', '热菜', '4');
INSERT INTO `foodtypeinfo` VALUES ('5', '甜食', '1');
INSERT INTO `foodtypeinfo` VALUES ('6', '冷食', '1');
INSERT INTO `foodtypeinfo` VALUES ('7', '凉菜', '1');
INSERT INTO `foodtypeinfo` VALUES ('8', '热菜', '1');
INSERT INTO `foodtypeinfo` VALUES ('9', '甜食', '2');
INSERT INTO `foodtypeinfo` VALUES ('10', '冷食', '2');
INSERT INTO `foodtypeinfo` VALUES ('11', '凉菜', '2');
INSERT INTO `foodtypeinfo` VALUES ('12', '热菜', '2');
INSERT INTO `foodtypeinfo` VALUES ('13', '甜食', '3');
INSERT INTO `foodtypeinfo` VALUES ('14', '冷食', '3');
INSERT INTO `foodtypeinfo` VALUES ('15', '凉菜', '3');
INSERT INTO `foodtypeinfo` VALUES ('16', '热菜', '3');

-- ----------------------------
-- Table structure for `orderdetailsinfo`
-- ----------------------------
DROP TABLE IF EXISTS `orderdetailsinfo`;
CREATE TABLE `orderdetailsinfo` (
  `orderDetailsId` int(11) NOT NULL AUTO_INCREMENT,
  `foodName` varchar(20) NOT NULL,
  `foodNumber` int(11) NOT NULL,
  `foodAttribute` varchar(20) DEFAULT NULL,
  `foodWeight` varchar(20) DEFAULT NULL,
  `price` decimal(10,0) NOT NULL,
  `totalPrice` decimal(10,0) NOT NULL,
  `orderId` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderDetailsId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orderdetailsinfo
-- ----------------------------
INSERT INTO `orderdetailsinfo` VALUES ('1', '麻婆豆腐', '3', '一般', '小份', '12', '36', '1');
INSERT INTO `orderdetailsinfo` VALUES ('2', '鸡肉拉皮', '2', '一般', '小份', '12', '24', '1');
INSERT INTO `orderdetailsinfo` VALUES ('3', '鱼香肉丝', '1', '一般', '小份', '12', '12', '1');
INSERT INTO `orderdetailsinfo` VALUES ('4', '鸡皮鲟龙', '1', '一般', '小份', '12', '12', '1');
INSERT INTO `orderdetailsinfo` VALUES ('5', '青瓜拼腰', '1', '一般', '小份', '12', '12', '2');
INSERT INTO `orderdetailsinfo` VALUES ('6', '夜合虾仁', '1', '一般', '小份', '12', '12', '2');
INSERT INTO `orderdetailsinfo` VALUES ('7', '鸡皮鲟龙', '1', '一般', '小份', '12', '12', '2');
INSERT INTO `orderdetailsinfo` VALUES ('8', '蚂蚁上树', '1', '一般', '小份', '12', '12', '2');
INSERT INTO `orderdetailsinfo` VALUES ('9', '1', '1', '1', '1', '1', '1', '0');

-- ----------------------------
-- Table structure for `orderinfo`
-- ----------------------------
DROP TABLE IF EXISTS `orderinfo`;
CREATE TABLE `orderinfo` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `orderRemarks` varchar(50) DEFAULT NULL,
  `orderTime` varchar(200) DEFAULT NULL,
  `seatId` int(11) DEFAULT NULL,
  `shopId` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orderinfo
-- ----------------------------
INSERT INTO `orderinfo` VALUES ('1', '多来两个套套', '2018-1-12', '1', '2', '0');
INSERT INTO `orderinfo` VALUES ('2', '多来两个套套', '2018-1-12', '1', '3', '1');
INSERT INTO `orderinfo` VALUES ('3', '1', '2018-07-21 19:19:10', '1', '1', '0');

-- ----------------------------
-- Table structure for `seatinfo`
-- ----------------------------
DROP TABLE IF EXISTS `seatinfo`;
CREATE TABLE `seatinfo` (
  `seatId` int(11) NOT NULL AUTO_INCREMENT,
  `seatName` varchar(10) DEFAULT NULL,
  `shopId` int(11) DEFAULT NULL,
  PRIMARY KEY (`seatId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seatinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `shopinfo`
-- ----------------------------
DROP TABLE IF EXISTS `shopinfo`;
CREATE TABLE `shopinfo` (
  `shopId` int(11) NOT NULL AUTO_INCREMENT,
  `shopName` varchar(20) NOT NULL,
  `shopIntroduction` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shopinfo
-- ----------------------------
INSERT INTO `shopinfo` VALUES ('1', '云翔商铺', '一家很皮的店铺');
INSERT INTO `shopinfo` VALUES ('2', '天天商铺', '极致奢华，拥有传统老手艺的店铺');
INSERT INTO `shopinfo` VALUES ('3', '疯子商铺', '不一样的感觉，不一般的享受，让你体会到舌尖的尖叫');
INSERT INTO `shopinfo` VALUES ('4', '赤炎商铺', '火辣辣的夏天，火辣辣的我，你选择的没错，就是我');
